"""Browse a data file with a GUI"""

__authors__ = ["V. Valls"]
__license__ = "MIT"
__date__ = "12/10/2018"

" 2020-01-31 Modified by Sang-Woo Kim, removed customDataWidget related codes"

import logging
import os.path

from silx.gui import qt
# from silx.gui.data.DataViewerFrame import DataViewerFrame
from silx.app.view.DataPanel import _HeaderLabel
from xicam.cdi.DataViewerFrame import DataViewerFrame


_logger = logging.getLogger(__name__)
class DataPanel(qt.QWidget):

    def __init__(self, parent=None, context=None):
        qt.QWidget.__init__(self)

        self.__dataTitle = _HeaderLabel(self)
        self.__dataTitle.setVisible(False)

        self.__dataViewer = DataViewerFrame(self)
        self.__dataViewer.setGlobalHooks(context)

        layout = qt.QVBoxLayout(self)
        layout.setSpacing(0)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.__dataTitle)
        layout.addWidget(self.__dataViewer)

    def getData(self):
        return self.__dataViewer.data()

    def setData(self, data):
        self.__customNxdataItem = None
        self.__dataViewer.setData(data)
        self.__dataTitle.setVisible(data is not None)
        if data is not None:
            self.__dataTitle.setVisible(True)
            if hasattr(data, "name"):
                if hasattr(data, "file"):
                    filename = str(data.file.filename)
                else:
                    filename = ""
                path = data.name
            else:
                filename = ""
                path = ""
            self.__dataTitle.setData(filename, path)

    def removeDatasetsFrom(self, root):
        """
        Remove all datasets provided by this root

        .. note:: This function do not update data stored inside
            customNxdataItem cause in the silx-view context this item is
            already updated on his own.

        :param root: The root file of datasets to remove
        """
        data = self.__dataViewer.data()
        if data is not None:
            if data.file is not None:
                # That's an approximation, IS can't be used as h5py generates
                # To objects for each requests to a node
                if data.file.filename == root.file.filename:
                    self.__dataViewer.setData(None)

    def replaceDatasetsFrom(self, removedH5, loadedH5):
        """
        Replace any dataset from any NXdata items using the same dataset name
        from another root.

        Usually used when a file was synchronized.

        .. note:: This function do not update data stored inside
            customNxdataItem cause in the silx-view context this item is
            already updated on his own.

        :param removedRoot: The h5py root file which is replaced
            (which have to be removed)
        :param loadedRoot: The new h5py root file which have to be used
            instread.
        """

        data = self.__dataViewer.data()
        if data is not None:
            if data.file is not None:
                if data.file.filename == removedH5.file.filename:
                    # Try to synchonize the viewed data
                    try:
                        # TODO: It have to update the data without changing the
                        # view which is not so easy
                        newData = loadedH5[data.name]
                        self.__dataViewer.setData(newData)
                    except Exception:
                        _logger.debug("Backtrace", exc_info=True)


def main():

    global app
    app = qt.QApplication([])

    # Create the ad hoc window containing a PlotWidget and associated tools
    window = DataPanel()
    window.setAttribute(qt.Qt.WA_DeleteOnClose)
    window.show()

    app.exec_()

if __name__ == '__main__':
    main()
