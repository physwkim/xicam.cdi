import sys
from os.path import expanduser

from silx.gui import qt

import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, \
                                    ParameterTree, \
                                    ParameterItem

from xicam.cdi.pathParameter import FilePathParameter, \
                                             DirPathParameter, \
                                             LabelParameter, \
                                             SimpleParameter, \
                                             TextParameter

class ReconstructionPanel(ParameterTree):
    sigDoRecon = qt.pyqtSignal()
    sigCancel = qt.pyqtSignal()
    """Reconstruction Settings panel"""

    def __init__(self, parent, *args, **kwargs):
        super(ReconstructionPanel, self).__init__(*args, **kwargs)
        self.parent = parent

        homePath = expanduser('~')

        # Load Image path
        self.imagePathParam = FilePathParameter(name='Image File Path',
                                                type='str',
                                                value=homePath,
                                                fileType='Image (*.tif *.tiff *.cxi *.npy)')

        # self.roiParam = SimpleParameter(name='ROI',
        #                                 type='str',
        #                                 value='')

        # Initial Support
        self.supportParams = LabelParameter(name='Support Parameters',
                                            type='str',
                                            readonly=True,
                                            value='')

        # Support Type
        self.supportType = pTypes.ListParameter(name='Type',
                                                type='list',
                                                values=['auto', 'circle', 'square'],
                                                value='square')
        # Support Size
        self.supportSize = SimpleParameter(name='Size',
                                           type='int',
                                           value='20')

        # Support Only Shrink
        self.supportOnlyShrink = SimpleParameter(name='Only Shink',
                                           type='bool',
                                           value=False)

        # Support threshold
        self.supportThreshold = SimpleParameter(name='Threshold',
                                                type='float',
                                                value='0.25')
        
        # Support threshold method
        self.supportThresholdMethod = pTypes.ListParameter(name='Threshold Method',
                                                     type='list',
                                                     values=['max', 'average', 'rms'],
                                                     value='rms')

        # Support smooth width begin
        self.supportSmoothWidthBegin = SimpleParameter(name='Smooth width begin',
                                                       type='float',
                                                       value=2)

        # Support smooth width end
        self.supportSmoothWidthEnd = SimpleParameter(name='Smooth width end',
                                                       type='float',
                                                       value=0.25)

        # Support smooth width relaxation number
        self.supportSmoothRelaxNum = SimpleParameter(name='Smooth width relaxation number',
                                                       type='int',
                                                       value=500)

        # Append to supportParams
        self.supportParams.addChild(self.supportType)
        self.supportParams.addChild(self.supportSize)
        self.supportParams.addChild(self.supportThresholdMethod)
        self.supportParams.addChild(self.supportThreshold)
        self.supportParams.addChild(self.supportSmoothWidthBegin)
        self.supportParams.addChild(self.supportSmoothWidthEnd)
        self.supportParams.addChild(self.supportSmoothRelaxNum)
        self.supportParams.addChild(self.supportOnlyShrink)

        # Inital Mask Parameters
        self.maskType = pTypes.ListParameter(name='Mask Type',
                                             type='list',
                                             values=['zero', 'negative', 'file', 'None'],
                                             value='zero')

        self.maskPath = FilePathParameter(name='Mask File Path',
                                          type='str',
                                          value='',
                                          fileType='Mask (*.cxi *.tif *.npy *.edf)')

        self.maskType.addChild(self.maskPath)

        # Inital Mask Parameters
        self.objectParams = LabelParameter(name='Object Parameters',
                                            type='str',
                                            readonly=True,
                                            value='')

        self.objectPath = FilePathParameter(name='Object File Path',
                                          type='str',
                                          value='',
                                          fileType='tif (*.tif);tiff (*.tiff);cxi (*.cxi);npy (*.npy)')

        self.positivity = SimpleParameter(name='Positivity',
                                          type='bool',
                                          value=True)


        self.objectParams.addChild(self.objectPath)
        self.objectParams.addChild(self.positivity)

        # Algorithm parameters
        self.algorithmParams = LabelParameter(name='Algorithm Parameters',
                                            type='str',
                                            readonly=True,
                                            value='')

        # Algoritms
        self.algorithm = SimpleParameter(name='Algoritms',
                                              type='str',
                                              value='(Sup*er**200)*(Sup*HIO**50)**10')
        # Number of run
        self.detwinParam = SimpleParameter(name='Detwin',
                                              type='bool',
                                              value=False)
        # Number of run
        self.numRunParam = SimpleParameter(name='Number of Run',
                                              type='int',
                                              value=1)

        # Crop output
        # self.cropOutputParam = SimpleParameter(name='Crop output',
        #                                       type='int',
        #                                       value=0)

        self.verbose = SimpleParameter(name='Update cycle(verbose)',
                                       type='int',
                                       value=50)

        self.algorithmParams.addChild(self.algorithm)
        self.algorithmParams.addChild(self.detwinParam)
        self.algorithmParams.addChild(self.numRunParam)
        # self.algorithmParams.addChild(self.cropOutputParam)
        self.algorithmParams.addChild(self.verbose)

        # Instruction
        self.instruction = TextParameter(name='Algorithm Instructions',
                                         readonly=True,
                                         value="Examples of algorithm strings, where steps are separated with commas (and NO SPACE!),\n" + \
                                                "and are applied from right to left. Operations in a given step will be applied \n" + \
                                                "mathematically, also from right to left, and **N means repeating N tymes (N cycles)\n" + \
                                                "the  operation on the left of the exponent:\n\n" + \
                                                "algorithm=(Sup*er**10*HIO**30*er**10)**50 \n\n" + \
                                                "algorithm=(Sup*er**50)**4*(Sup*HIO**50)**14 \n\n" + \
                                                "algorithm=HIO : single HIO cycle algorithm=ER**100 : 100 cycles of HIO\n\n" + \
                                                "algorithm=ER**50,HIO**100 : 100 cycles of HIO, followed by 50 cycles of ER\n\n" + \
                                                "algorithm=ER**50*HIO**100 : 100 cycles of HIO, followed by 50 cycles of ER\n\n" + \
                                                "algorithm=ER**50,(Sup*ER**5*HIO**50)**10 : 10 times [50 HIO + 5 ER + Support update]," + \
                                                " followed by 50 ER \n\nalgorithm=ER**50,verbose=1,(Sup*ER**5*HIO**50)**10,verbose=100," + \
                                                "HIO**100: change the periodicity of verbose output\n\n" + \
                                                "algorithm=ER**50,(Sup*ER**5*HIO**50)**10,support_post_expand=1, (Sup*ER**5*HIO**50)**10," + \
                                                "support_post_expand=-1#2,HIO**100 : same but change the post-expand (wrap) method\n\n " + \
                                                "algorithm=ER**50,(Sup*PSF*ER**5*HIO**50)**5,(Sup*ER**5*HIO**50)**10,HIO**100 : activate" + \
                                                "partial correlation after a first series of algorithms\n\n" +\
                                                "algorithm=ER**50,(Sup*PSF*HIO**50)**4,(Sup*HIO**50)**8 : typical algorithm steps with partial coherence\n\n" + \
                                                "algorithm=ER**50,(Sup*HIO**50)**4,(Sup*HIO**50)**4,positivity=0,(Sup*HIO**50)**8,positivity=1 : same as previous" + \
                                                " but starting with positivity constraint, removed at the end.\n\n")

        # Reconstruction Button
        self.reconstructionAction = pTypes.ActionParameter(name='Reconstruct')
        self.reconstructionAction.sigActivated.connect(self.sigDoRecon)

        # Cancel Button
        self.cancelAction = pTypes.ActionParameter(name='Cancel')
        self.cancelAction.sigActivated.connect(self.sigCancel)

        self.parameter = pTypes.GroupParameter(name='Reconstruction',
                                               children=[self.imagePathParam,
                                                         self.supportParams,
                                                         self.objectParams,
                                                         self.maskType,
                                                         self.algorithmParams,
                                                         self.instruction,
                                                         self.reconstructionAction,
                                                         self.cancelAction]) 
        self.setParameters(self.parameter, showTop=False)


