#! /opt/local/bin/python
# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2017-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr

import sys
import time
import timeit
import numpy as np
import traceback
import warnings
import os
from PIL import Image
import fabio
import zmq
import h5py

from silx.io.specfile import SpecFile

from scipy.fftpack import fftn, ifftn, fftshift
from scipy.ndimage.measurements import center_of_mass
from scipy.io import loadmat
from skimage.external import tifffile

from pynx.cdi.runner import CDIRunner, CDIRunnerException, CDIRunnerScan, params_generic
from pynx.cdi.cu_operator import SupportUpdate, DetwinHIO, DetwinRAAR
from pynx.cdi.cu_operator import FourierApplyAmplitude, EstimatePSF, FreePU
from pynx.cdi.cu_operator import AutoCorrelationSupport, ScaleObj

from xicam.cdi.workflows.Algorithm import CDI, HIO, ER, CF, RAAR, ML, GPS, ShowCDI

params_beamline = {'auto_center_resize': False, 'support_type': 'circle', 'detwin': False, 'support_size': 50,
                   'nb_raar': 0, 'nb_hio': 600, 'nb_er': 200, 'nb_ml': 0, 'instrument': 'PAL BL9C', 'mask': None,
                   'zero_mask': 0, 'positivity': True, 'specfile' : None}

helptext_beamline = """
Script to perform a CDI reconstruction of data from BL9C@PLS-2
command-line/file parameters arguments: (all keywords are case-insensitive):

    Specific defaults for this script:
        auto_center_resize = True
        detwin = True
        nb_raar = 600
        nb_hio = 0
        nb_er = 200
        nb_ml = 0
        support_size = None
        zero_mask = auto
"""

params = params_generic.copy()
for k, v in params_beamline.items():
    params[k] = v


class CDIRunnerScanBL9C(CDIRunnerScan):
    def __init__(self, params, scan):
        super(CDIRunnerScanBL9C, self).__init__(params, scan)

        #ZeroMQ Context
        CONTEXT = zmq.Context()
        self.sock = CONTEXT.socket(zmq.PUB)
        port = params['zmq_log_port']
        self.sock.connect("tcp://localhost:"+str(port))

        port_img = params['zmq_img_port']
        self.sock_img = CONTEXT.socket(zmq.PUB)
        self.sock_img.connect("tcp://localhost:"+str(port_img))

    def _print(self, msg, *args):
        """print msg to stdout and zmq port"""
        # print a string to stdout
        print(msg)
        # send to zmq
        if type(msg) == str:
            self.sock.send(msg.encode())

        for msg in args:
            if type(msg) == str:
                # print a string to stdout
                print(msg)
                # send to zmq
                self.sock.send(msg.encode())
            else:
                print(str(msg))

    def load_data(self):
        """
        Loads data. If no bl9c-specific keywords have been supplied, use the default data loading.

        """
        if self.params['specfile'] is None or self.scan is None:
            filename = self.params['data']
            self._print('Loading data: ' + filename)
            ext = os.path.splitext(filename)[-1]
            if ext == '.npy':
                self.iobs = np.load(filename).astype(np.float32)
            elif ext == '.edf':
                self.iobs = fabio.open(filename).data.astype(np.float32)
            elif ext == '.tif' or ext == '.tiff':
                # frames = Image.open(filename)
                frames = tifffile.imread(filename)
                if len(frames.shape) == 3:
                    nz, ny, nx = frames.shape
                    self.iobs = np.empty((nz, ny, nx), dtype=np.float32)
                    for i in range(nz):
                        self.iobs[i] = np.array(frames[i])
                else:
                    self.iobs = np.array(frames)
            elif ext == '.npz':
                d = np.load(filename)
                if 'data' in d.keys():
                    self.iobs = d['data'].astype(np.float32)
                elif 'iobs' in d.keys():
                    self.iobs = d['iobs'].astype(np.float32)
                else:
                    # Assume only the data is in the datafile
                    for k, v in d.items():
                        self.iobs = v.astype(np.float32)
                        break
            elif ext == '.cxi':
                cxi = h5py.File(filename, 'r')
                if '/entry_1/instrument_1/source_1/energy' in cxi:
                    nrj = cxi['/entry_1/instrument_1/source_1/energy'][()] / 1.60218e-16
                    self.params['wavelength'] = 12.384 / nrj * 1e-10
                    self._print("  CXI input: Energy = %8.2fkeV" % nrj)
                if '/entry_1/instrument_1/detector_1/distance' in cxi:
                    self.params['detector_distance'] = cxi['/entry_1/instrument_1/detector_1/distance'][()]
                    self._print("  CXI input: detector distance = %8.2fm" % self.params['detector_distance'])
                if '/entry_1/instrument_1/detector_1/x_pixel_size' in cxi:
                    self.params['pixel_size_detector'] = cxi['/entry_1/instrument_1/detector_1/x_pixel_size'][()]
                    self._print("  CXI input: detector pixel size = %8.2fum" % (self.params['pixel_size_detector'] * 1e6))
                self._print("  CXI input: loading iobs")
                if 'entry_1/instrument_1/detector_1/data' in cxi:
                    self.iobs = cxi['entry_1/instrument_1/detector_1/data'][()].astype(np.float32)
                elif 'entry_1/image_1/data' in cxi:
                    self.iobs = cxi['entry_1/image_1/data'][()].astype(np.float32)
                    if 'entry_1/image_1/is_fft_shifted' in cxi:
                        if cxi['entry_1/image_1/is_fft_shifted'][()] > 0:
                            self.iobs = fftshift(self.iobs)
                else:
                    self.iobs = cxi['entry_1/data_1/data'][()].astype(np.float32)
                if 'entry_1/instrument_1/detector_1/mask' in cxi:
                    self.mask = cxi['entry_1/instrument_1/detector_1/mask'][()].astype(np.int8)
                    nb = self.mask.sum()
                    self._print("  CXI input: loading mask, with %d pixels masked (%6.3f%%)" % (nb, nb * 100 / self.mask.size))

                if 'entry_1/data_1/process_1/configuration' in cxi:
                    # Load specific parameters from CXI file and carry on some parameters
                    # TODO: this is a bit of a kludge, find a more maintainable way to decide which parameters to keep..
                    for k, v in cxi['entry_1/data_1/process_1/configuration'].items():
                        vv = None
                        if k in self.params:
                            vv = self.params[k]
                        if (k not in self.params.keys() or (k in ['instrument', 'sample_name', 'scan', 'imgname']
                                                            and vv is None)) and v is not None:
                            if type(v) is h5py.Group:
                                self.params[k] = {}
                                for dk, dv in v.items():
                                    self.params[k][dk] = dv[...]
                            else:
                                self.params[k] = v[...]

            self._print("Finished loading iobs data, with size:" + str(self.iobs.size))

        else:
            if isinstance(self.scan, str):
                # do we combine several scans ?
                vs = self.scan.split('+')
            else:
                vs = [self.scan]
            imgn = None
            scan_motor_last_value = None
            for scan in vs:
                if scan is None:
                    scan = 0
                else:
                    scan = int(scan)
                s = SpecFile(self.params['specfile'])['%d.1' % (scan)]
                h = s.scan_header_dict

                if self.params['imgcounter'] == 'auto':
                    if 'ei2minr' in s.labels:
                        self.params['imgcounter'] = 'ei2minr'
                    elif 'mpx4inr' in s.labels:
                        self.params['imgcounter'] = 'mpx4inr'
                    self._print("Using image counter: %s" % (self.params['imgcounter']))

                if self.params['wavelength'] is None and 'UMONO' in h:
                    nrj = float(h['UMONO'].split('mononrj=')[1].split('ke')[0])
                    w = 12.384 / nrj
                    self.params['wavelength'] = w
                    self._print("Reading nrj from spec data: nrj=%6.3fkeV, wavelength=%6.3fA" % (nrj, w))

                if 'UDETCALIB' in h:
                    # UDETCALIB cen_pix_x=18.347,cen_pix_y=278.971,pixperdeg=445.001,det_distance_CC=1402.175,
                    # det_distance_COM=1401.096,timestamp=20170926...
                    if self.params['detector_distance'] is None:
                        if 'det_distance_CC=' in h['UDETCALIB']:
                            self.params['detector_distance'] = float(h['UDETCALIB'].split('stance_CC=')[1].split(',')[0])
                            self._print("Reading detector distance from spec data: %6.3fm" % self.params['detector_distance'])
                        else:
                            self._print('No detector distance given. No det_distance_CC in UDETCALIB ??: %s' % (h['UDETCALIB']))
                # Also save specfile parameters as a 'user_' param
                self.params['user_bl9c_specfile_header'] = s.file_header_dict
                self.params['user_bl9c_specfile_scan_header'] = s.scan_header_dict

                # Read images
                if imgn is None:
                    imgn = s.data_column_by_name(self.params['imgcounter']).astype(np.int)
                else:
                    # try to be smart: exclude first image if first motor position is at the end of last scan
                    if scan_motor_last_value == s.data[0, 0]:
                        i0 = 0
                    else:
                        self._print("Scan %d: excluding first image at same position as previous one" % (scan))
                        i0 = 1
                    imgn = np.append(imgn, s.data_column_by_name(self.params['imgcounter'])[i0:].astype(np.int))
                scan_motor_last_value = s.data[0, 0]
            self.iobs = None
            t0 = timeit.default_timer()
            if self.params['imgname'] == None:
                if 'ULIMA_eiger2M' in h:
                    imgname = h['ULIMA_eiger2M'].strip().split('_eiger2M_')[0] + '_eiger2M_%05d.edf.gz'
                    self._print("Using Eiger 2M detector images: %s" % (imgname))
                else:
                    imgname = h['ULIMA_mpx4'].strip().split('_mpx4_')[0] + '_mpx4_%05d.edf.gz'
                    self._print("Using Maxipix mpx4 detector images: %s" % (imgname))
            else:
                imgname = self.params['imgname']
            sys.stdout.write('Reading frames: ')
            ii = 0
            for i in imgn:
                if (i - imgn[0]) % 20 == 0:
                    sys.stdout.write('%d ' % (i - imgn[0]))
                    sys.stdout.flush()
                frame = fabio.open(imgname % i).data
                if self.iobs is None:
                    self.iobs = np.empty((len(imgn), frame.shape[0], frame.shape[1]), dtype=frame.dtype)
                self.iobs[ii] = frame
                ii += 1
            self._print("\n")
            dt = timeit.default_timer() - t0
            self._print('Time to read all frames: %4.1fs [%5.2f Mpixel/s]' % (dt, self.iobs.size / 1e6 / dt))

    def prepare_cdi(self, free_pixel_mask=None):
        """
        Prepare CDI object from input data.

        :param free_pixel_mask: optionally supply a free pixel mask to keep it for successive runs
        :return: nothing. Creates self.cdi object.
        """
        self.init_object()
        pix = self.params['pixel_size_detector']
        d = self.params['detector_distance']
        w = self.params['wavelength']
        m = self.mask
        if m is not None:
            m = fftshift(m)
        sup = None
        if self.support is not None:
            sup = fftshift(self.support)
        self.cdi = CDI(self.sock, iobs=fftshift(self.iobs), support=sup, mask=m, pixel_size_detector=pix,
                       wavelength=w, detector_distance=d, obj=fftshift(self.obj))
        if self.params['support'] in ['auto'] and self.params['support_formula'] is None:
            r = self.params['support_autocorrelation_threshold']
            self._print("Using auto-correlation to estimate initial support, relative threshold = %5.3f" % r)
            self.cdi = AutoCorrelationSupport(r) * self.cdi
            if self.params['object'] is None:
                self.cdi.set_obj(self.cdi.get_obj() * self.cdi.get_support())
        if free_pixel_mask is None:
            if self.params['free_pixel_mask'] is not None:
                filename = self.params['free_pixel_mask']
                self._print('Loading free pixel mask from: ', filename)
                ext = os.path.splitext(filename)[-1]
                if ext == '.npy':
                    free_pixel_mask = np.load(filename).astype(np.bool)
                elif ext == '.tif' or ext == '.tiff':
                    frames = Image.open(filename)
                    frames.seek(0)
                    free_pixel_mask = np.array(frames).astype(np.bool)
                elif ext == '.mat':
                    a = list(loadmat(filename).values())
                    for v in a:
                        if np.size(v) == self.iobs.size:
                            # Avoid matlab strings and attributes, and get the array
                            free_pixel_mask = np.array(v).astype(np.bool)
                            break
                elif ext == '.npz':
                    d = np.load(filename)
                    if 'free_pixel_mask' in d.keys():
                        free_pixel_mask = d['free_pixel_mask'].astype(np.bool)
                    else:
                        # Assume only the mask is in the datafile
                        for k, v in d.items():
                            if v.shape == self.iobs.shape:
                                free_pixel_mask = v.astype(np.bool)
                            break
                elif ext == '.edf':
                    free_pixel_mask = fabio.open(filename).data.astype(np.bool)
                elif ext == '.cxi':
                    fh5 = h5py.File(filename, mode='r')
                    if '/entry_last/image_1/process_1/configuration' in fh5:
                        free_pixel_mask = fh5['/entry_last/image_1/process_1/configuration/free_pixel_mask'][()]
                    else:
                        raise CDIRunnerException("Supplied free_pixel_mask=%s, but /entry_last/image_1/process_1/"
                                                 "configuration/free_pixel_mask not found !" % filename)
                else:
                    raise CDIRunnerException(
                        "Supplied mask=%s, but format not recognized (recognized .npy, .npz, .tif, .edf, .mat)" % filename)
                self.cdi.set_free_pixel_mask(free_pixel_mask, verbose=True, shift=True)
            else:
                self.cdi.init_free_pixels(ratio=5e-2, island_radius=3, exclude_zone_center=0.05, verbose=True)
        else:
            self.cdi.set_free_pixel_mask(free_pixel_mask, verbose=True)

        # Scale initial object
        self.cdi = ScaleObj(method='F') * self.cdi

    def run(self, file_name=None):
        """
        Main work function. Will run selected algorithms according to parameters

        :param file_name: output file_name. If None, the result is not saved.
        :return:
        """
        if self.params['algorithm'] is not None:
            self.run_algorithm(self.params['algorithm'], file_name=file_name)
            return

        # We did not get an algorithm string, so create the chain of operators from individual parameters
        nb_raar = self.params['nb_raar']
        nb_hio = self.params['nb_hio']
        nb_er = self.params['nb_er']
        nb_ml = self.params['nb_ml']
        positivity = self.params['positivity']
        support_only_shrink = self.params['support_only_shrink']
        beta = self.params['beta']
        detwin = self.params['detwin']
        psf = self.params['psf']  # Experimental, to take into account partial coherence
        live_plot = self.params['live_plot']
        support_update_period = self.params['support_update_period']
        support_smooth_width_begin = self.params['support_smooth_width_begin']
        support_smooth_width_end = self.params['support_smooth_width_end']
        support_threshold = self.params['support_threshold']
        support_threshold_method = self.params['support_threshold_method']
        support_post_expand = self.params['support_post_expand']
        support_update_border_n = self.params['support_update_border_n']
        verbose = self.params['verbose']
        zero_mask = self.params['zero_mask']
        if live_plot is True:
            live_plot = verbose
        elif live_plot is False:
            live_plot = 0

        if nb_ml > 0 and psf:
            self._print("ML deactivated - PSF is unimplemented in ML")
            nb_ml = 0

        self._print('No algorithm chain supplied. Proceeding with the following parameters:')
        self._print(' %30s = ' % 'nb_hio', nb_hio)
        self._print(' %30s = ' % 'nb_raar', nb_raar)
        self._print(' %30s = ' % 'nb_er', nb_er)
        self._print(' %30s = ' % 'nb_ml', nb_ml)
        self._print(' %30s = ' % 'positivity', positivity)
        self._print(' %30s = ' % 'support_only_shrink', support_only_shrink)
        self._print(' %30s = ' % 'beta', beta)
        self._print(' %30s = ' % 'detwin', detwin)
        self._print(' %30s = ' % 'live_plot', live_plot)
        self._print(' %30s = ' % 'support_update_period', support_update_period)
        self._print(' %30s = ' % 'support_smooth_width_begin', support_smooth_width_begin)
        self._print(' %30s = ' % 'support_smooth_width_end', support_smooth_width_end)
        self._print(' %30s = ' % 'support_threshold', support_threshold)
        self._print(' %30s = ' % 'support_threshold_method', support_threshold_method)
        self._print(' %30s = ' % 'support_post_expand', support_post_expand)
        if support_update_border_n:
            self._print(' %30s = ' % 'support_update_border_n', support_update_border_n)
        self._print(' %30s = ' % 'zero_mask', zero_mask)
        self._print(' %30s = ' % 'verbose', verbose)

        # Write an algorithm string based on the given parameters
        # Begin by listing all cycles for which there is a new operator applied
        cycles = [nb_hio + nb_raar + nb_er + nb_ml]
        if nb_hio > 0:
            cycles.append(nb_hio)
        if nb_raar > 0:
            cycles.append(nb_hio + nb_raar)
        if nb_er > 0:
            cycles.append(nb_hio + nb_raar + nb_er)
        if nb_ml > 0:
            cycles.append(nb_hio + nb_raar + nb_er + nb_ml)
        if support_update_period > 0:
            cycles += list(range(0, nb_hio + nb_raar + nb_er, support_update_period))
        if detwin:
            detwin_cycles = int((nb_hio + nb_raar) // 4)
            cycles.append(detwin_cycles)
        if psf:
            psf_0 = int((nb_raar + nb_hio) * 0.75)
            psf_cycles = list(range(psf_0, nb_hio + nb_raar + nb_er, 50))
            cycles += psf_cycles

        if type(zero_mask) is str:
            if zero_mask.lower() == 'auto':
                zm = 1
                zm_cycle = int((nb_hio + nb_raar) * 0.6)
                cycles.append(zm_cycle)
            else:
                zm_cycle = None
                if zero_mask.lower() == 'true' or zero_mask.lower() == '1':
                    zm = True
                else:
                    zm = False
        else:
            zm_cycle = None
            zm = zero_mask  # Can be 0, 1, True, False..

        # self._print("cycles before = "+str(cycles))
        cycles.sort()
        # self._print("cycles after = "+str(cycles))

        algo = 1  # Start with unity operator
        algo_str = []
        while len(cycles) > 1:
            i0, i1 = cycles[:2]
            n = i1 - i0
            cycles.pop(0)

            if i0 == i1:
                continue

            if zm_cycle is not None:
                if i1 == zm_cycle:
                    # Free masked pixels
                    zm = 0
                    self._print('Switching from zero_mask=1 to 0 after %d cycles' % zm_cycle)
            if i1 <= nb_hio:
                algo1 = HIO(self.sock_img, beta=beta, positivity=positivity, calc_llk=verbose, show_cdi=live_plot,
                            zero_mask=zm) ** n
            elif nb_raar > 0 and i1 <= (nb_hio + nb_raar):
                algo1 = RAAR(self.sock_img, beta=beta, positivity=positivity, calc_llk=verbose, show_cdi=live_plot,
                             zero_mask=zm) ** n
            elif nb_er > 0 and i1 <= (nb_hio + nb_raar + nb_er):
                algo1 = ER(self.sock_img, positivity=positivity, calc_llk=verbose, show_cdi=live_plot, zero_mask=zm) ** n
            elif nb_ml > 0 and i1 <= (nb_hio + nb_raar + nb_er + nb_ml):
                algo1 = ML(self.sock_img, calc_llk=verbose, show_cdi=live_plot) ** n * FourierApplyAmplitude(calc_llk=True,
                                                                                              zero_mask=zm)

            if support_update_period > 0:
                if i1 % support_update_period == 0 and len(cycles) > 1:
                    s = support_smooth_width_begin, support_smooth_width_end, nb_raar + nb_hio
                    algo1 = SupportUpdate(threshold_relative=support_threshold, smooth_width=s,
                                          force_shrink=support_only_shrink, method=support_threshold_method,
                                          post_expand=support_post_expand,
                                          update_border_n=support_update_border_n) * algo1

            if detwin:
                if i1 == detwin_cycles:
                    if i1 <= nb_hio:
                        algo1 = DetwinHIO(beta=beta, positivity=False, nb_cycle=10, zero_mask=zm) * algo1
                    else:
                        algo1 = DetwinRAAR(beta=beta, positivity=False, nb_cycle=10, zero_mask=zm) * algo1

            if psf:
                if i1 in psf_cycles and len(cycles) > 1:
                    algo1 = EstimatePSF() ** 100 * algo1
            algo_str.append(str(algo1))
            if algo is None:
                algo = algo1
            else:
                algo = algo1 * algo

        # Finish by FourierApplyAmplitude
        algo = FourierApplyAmplitude(calc_llk=True, zero_mask=zm) * algo

        # print("Algorithm chain: ", algo)

        # More pretty printing:
        s = ''
        while len(algo_str):
            s0 = algo_str.pop(0)
            n = 1
            while len(algo_str):
                if algo_str[0] == s0:
                    del algo_str[0]
                    n += 1
                else:
                    break
            if s != '':
                s = ' * ' + s
            if n > 1:
                s = '(%s)**%d' % (s0, n) + s
            else:
                s = s0 + s

        # Reformat chain so that it can be re-used from the command-line
        s = s.replace('()', '')
        s = s.replace('SupportUpdate', 'Sup')
        s = s.replace('EstimatePSF', 'PSF')

        self._print("Algorithm chain: ", s)

        # Now execute the recipe !
        t0 = timeit.default_timer()
        self.cdi = algo * self.cdi

        # Free GPU memory
        self.cdi = FreePU() * self.cdi

        self._print("\nTotal elapsed time for algorithms: %8.2fs" % (timeit.default_timer() - t0))
        if file_name is not None:
            self.save_result(file_name)

    def run_algorithm(self, algo_string, file_name=None):
        """
        Run a single or suite of algorithms in a given run.

        :param algo_string: a single or suite of algorithm steps to use, which can either correspond to a change
                            of parameters, i.e. 'beta=0.5', 'support_threshold=0.3', 'positivity=1',
                            or operators which should be applied to the cdi object, such as:
                            'hio**20': 20 cycles of HIO
                            'er': a single cycle or error reduction
                            'detwinhio**20': 20 cycles of hio after halving the support
                            'er**20*hio**100': 100 cycles of hio followed by 20 cycles of ER
                            '(sup*er**10*raar**100)**10': 10 times [100 cycles of RAAR + 10 ER + Support update]
        :param file_name: output file_name. If None, the result is not saved.
        """
        algo_split = algo_string.split(',')
        algo_split.reverse()
        self._print(algo_split)
        t0 = timeit.default_timer()
        for algo in algo_split:
            if self._algo_s == "":
                self._algo_s += algo
            else:
                self._algo_s += ',' + algo
            # TODO: keep track of run number
            self._print("\n", "#" * 100, "\n#", "\n#         Run: %g , Algorithm: %s\n#\n" % (1, algo), "#" * 100)
            realoptim = False  # Is this a real optimization (HIO, ER, ML, RAAR,...), or just a change of parameter ?

            if algo.lower().find('beta=') >= 0:
                self.params['beta'] = float(algo.lower().split('beta=')[-1])
            elif algo.lower().find('support_threshold=') >= 0:
                self.params['support_threshold'] = float(algo.lower().split('support_threshold=')[-1])
            elif algo.lower().find('support_threshold_mult=') >= 0:
                self.params['support_threshold'] *= float(algo.lower().split('support_threshold_mult=')[-1])
            elif algo.lower().find('gps_inertia=') >= 0:
                self.params['gps_inertia'] = float(algo.lower().split('gps_inertia=')[-1])
            elif algo.lower().find('gps_t=') >= 0:
                self.params['gps_t'] = float(algo.lower().split('gps_t=')[-1])
            elif algo.lower().find('gps_s=') >= 0:
                self.params['gps_s'] = float(algo.lower().split('gps_s=')[-1])
            elif algo.lower().find('gps_sigma_o=') >= 0:
                self.params['gps_sigma_o'] = float(algo.lower().split('gps_sigma_o=')[-1])
            elif algo.lower().find('gps_sigma_f=') >= 0:
                self.params['gps_sigma_f'] = float(algo.lower().split('gps_sigma_f=')[-1])
            elif algo.lower().find('positivity=') >= 0:
                self.params['positivity'] = int(algo.lower().split('positivity=')[-1])
            elif algo.lower().find('zero_mask=') >= 0:
                self.params['zero_mask'] = int(algo.lower().split('zero_mask=')[-1])
            elif algo.lower().find('support_only_shrink=') >= 0:
                self.params['support_only_shrink'] = int(algo.lower().split('support_only_shrink=')[-1])
            elif algo.lower().find('positivity=') >= 0:
                self.params['positivity'] = float(algo.lower().split('positivity=')[-1])
            elif algo.lower().find('support_smooth_width_begin=') >= 0:
                self.params['support_smooth_width_begin'] = float(algo.lower().split('support_smooth_width_begin=')[-1])
            elif algo.lower().find('support_smooth_width_end=') >= 0:
                self.params['support_smooth_width_end'] = float(algo.lower().split('positivity=')[-1])
            elif algo.lower().find('support_post_expand=') >= 0:
                s = algo.lower().split('support_post_expand=')[-1].replace('#', ',')
                self.params['support_post_expand'] = eval(s)
            elif algo.lower().find('support_threshold_method=') >= 0:
                self.params['support_threshold_method'] = algo.lower().split('support_post_expand=')[-1]
            elif algo.lower().find('support_update_border_n=') >= 0:
                self.params['support_update_border_n'] = int(algo.lower().split('support_update_border_n=')[-1])
            elif algo.lower().find('verbose=') >= 0:
                self.params['verbose'] = int(algo.lower().split('verbose=')[-1])
            elif algo.lower().find('live_plot=') >= 0:
                self.params['live_plot'] = int(algo.lower().split('live_plot=')[-1])
            elif algo.lower().find('fig_num=') >= 0:
                self.params['fig_num'] = int(algo.lower().split('fig_num=')[-1])
            elif algo.lower().find('manual') >= 0:
                pass  # Dummy type of algorithm
            else:
                # Not a keyword, so this must be an algorithm to run. Finally !
                realoptim = True
                positivity = self.params['positivity']
                support_only_shrink = self.params['support_only_shrink']
                beta = self.params['beta']
                support_smooth_width_begin = self.params['support_smooth_width_begin']
                support_smooth_width_end = self.params['support_smooth_width_end']
                support_smooth_width_relax_n = self.params['support_smooth_width_relax_n']
                smooth_width = support_smooth_width_begin, support_smooth_width_end, support_smooth_width_relax_n
                support_threshold = self.params['support_threshold']
                gps_inertia = self.params['gps_inertia']
                gps_t = self.params['gps_t']
                gps_s = self.params['gps_s']
                gps_sigma_o = self.params['gps_sigma_o']
                gps_sigma_f = self.params['gps_sigma_f']
                support_threshold_method = self.params['support_threshold_method']
                support_post_expand = self.params['support_post_expand']
                support_update_border_n = self.params['support_update_border_n']
                verbose = int(self.params['verbose'])
                live_plot = self.params['live_plot']
                fig_num = self.params['fig_num']
                zm = self.params['zero_mask']
                if type(zm) is str:
                    if zm.lower() == 'auto':
                        # TODO: better handle zero_mask='auto' for custom algorithms ?
                        zm = False
                    else:
                        if zm.lower() == 'true' or zm.lower() == '1':
                            zm = True
                        else:
                            zm = False
                if int(live_plot) == 1:  # That's for live_plot=True
                    live_plot = verbose

                # Create elementary operators
                fap = FourierApplyAmplitude(calc_llk=True, zero_mask=zm)
                er = ER(self.sock_img, positivity=positivity, calc_llk=verbose, show_cdi=live_plot, fig_num=fig_num, zero_mask=zm)
                hio = HIO(self.sock_img, beta=beta, positivity=positivity, calc_llk=verbose, show_cdi=live_plot, fig_num=fig_num,
                          zero_mask=zm)
                raar = RAAR(self.sock_img, beta=beta, positivity=positivity, calc_llk=verbose, show_cdi=live_plot, fig_num=fig_num,
                            zero_mask=zm)
                gps = GPS(self.sock_img, inertia=gps_inertia, t=gps_t, s=gps_s, sigma_f=gps_sigma_f, sigma_o=gps_sigma_o,
                          positivity=positivity, calc_llk=verbose, show_cdi=live_plot, fig_num=fig_num, zero_mask=zm)
                detwinhio = DetwinHIO(detwin_axis=0, beta=beta, positivity=positivity, zero_mask=zm)
                detwinhio1 = DetwinHIO(detwin_axis=1, beta=beta, positivity=positivity, zero_mask=zm)
                detwinhio2 = DetwinHIO(detwin_axis=2, beta=beta, positivity=positivity, zero_mask=zm)
                detwinraar = DetwinRAAR(detwin_axis=0, beta=beta, positivity=positivity, zero_mask=zm)
                detwinraar1 = DetwinRAAR(detwin_axis=1, beta=beta, positivity=positivity, zero_mask=zm)
                detwinraar2 = DetwinRAAR(detwin_axis=2, beta=beta, positivity=positivity, zero_mask=zm)
                cf = CF(self.sock_img, positivity=positivity, calc_llk=verbose, show_cdi=live_plot, fig_num=fig_num, zero_mask=zm)
                ml = ML(self.sock_img, calc_llk=verbose, show_cdi=live_plot, fig_num=fig_num) * fap
                psf = EstimatePSF(nb_cycle=100)
                estimatepsf = psf
                sup = SupportUpdate(threshold_relative=support_threshold, smooth_width=smooth_width,
                                    force_shrink=support_only_shrink, post_expand=support_post_expand,
                                    method=support_threshold_method, update_border_n=support_update_border_n)
                supportupdate = sup
                showcdi = ShowCDI(self.sock_img)

                try:
                    ops = eval(algo.lower())
                    self.cdi = ops * self.cdi
                except Exception as ex:
                    self._print(traceback.format_exc())
                    self._print(self.help_text)
                    self._print('\n\n Caught exception for scan %d: %s    \n' % (self.scan, str(ex)))
                    self._print('Could not interpret operator-based algorithm: ', algo)
                    # TODO: print valid examples of algorithms
                    sys.exit(1)
            if self.params['save'] == 'all' and realoptim and file_name is not None:
                # Finish with FourierApplyAmplitude
                self.cdi = FourierApplyAmplitude(calc_llk=True, zero_mask=zm) * self.cdi
                self.save_result(file_name)

        if self.params['save'] != 'all' and file_name is not None:
            # Finish with FourierApplyAmplitude
            zm = self.params['zero_mask']
            if type(zm) is str:
                if zm.lower() == 'auto':
                    zm = False
                else:
                    if zm.lower() == 'true' or zm.lower() == '1':
                        zm = True
                    else:
                        zm = False
            self.cdi = FourierApplyAmplitude(calc_llk=True, zero_mask=zm) * self.cdi
            self.save_result(file_name)
        self._print("\nTotal elapsed time for algorithms: %8.2fs" % (timeit.default_timer() - t0))
        # Free GPU memory
        self.cdi = FreePU() * self.cdi


    def save_result(self, file_name=None, verbose=True):
        """
        Save the results (object, support,...) at the end of a run
        :param file_name: the filename to save the data to. If its extension is either npz or cxi, this will
                          override params['output_format']. Otherwise, the extension will be replaced. Note that
                          the full filename will be appended with the llk value.
        :return:
        """
        if self.params['output_format'].lower() == 'none':
            return

        if file_name is None:
            file_name = "Result.%s" % self.params['output_format']

        filename, ext = os.path.splitext(file_name)

        if len(ext) < 2:
            ext = "." + self.params['output_format']

        llk = self.cdi.get_llk(normalized=True)

        if llk is not None and self.params['save'].lower() != 'all':
            filename += "_LLKf%08.4f_LLK%08.4f" % (llk[3], llk[0])

        filename += "_SupportThreshold%7.5f" % self.params['support_threshold']

        if ext not in ['.npz', '.cxi']:
            warnings.warn("CDI.save_result(): output format (%s) not understood, defaulting to CXI" % ext)
            ext = '.cxi'
        filename += ext

        if os.path.isfile(filename) and self.params['save'].lower() != 'all':
            warnings.warn("CDI.save_result(): output file already exists, not overwriting: %s" % filename)
            # raise CDIRunnerException("ERROR: output file already exists, no overwriting: %s" % filename)
            filename = filename[:-4]
            nn = 1
            while os.path.isfile("%s_%02d%s" % (filename, nn, ext)):
                nn += 1
            filename = "%s_%02d%s" % (filename, nn, ext)

        if verbose:
            self._print("Saving result to: %s" % filename)

        if ext == '.npz':
            np.savez_compressed(filename, obj=self.cdi.get_obj(shift=True))
        else:
            process = {}

            if self.params['algorithm'] is not None:
                process["algorithm"] = self.params['algorithm']

            params_string = ""
            for p in self.params.items():
                k, v = p
                if v is not None and k not in ['output_format']:
                    params_string += "%s = %s\n" % (k, str(v))

            process["note"] = {'configuration parameters used for phasing': params_string}

            append = False
            if self.params['save'].lower() == 'all':
                append = True

            self.cdi.save_obj_cxi(filename, sample_name=self.params['sample_name'], experiment_id=None,
                                  instrument=self.params['instrument'], note=self.params['note'],
                                  crop=int(self.params['crop_output']), process_notes=process,
                                  process_parameters=self.params, append=append)
            if False:
                try:
                    sf = os.path.split(filename)
                    os.system('ln -sf "%s" %s' % (sf[1], os.path.join(sf[0], 'latest.cxi')))
                except:
                    pass

        llk = self.cdi.get_llk()
        d = {'file_name': filename, 'llk_poisson': llk[0], 'llk_gaussian': llk[1], 'llk_euclidian': llk[2],
             'llk_poisson_free': llk[3], 'llk_gaussian_free': llk[4], 'llk_euclidian_free': llk[5],
             'nb_point_support': self.cdi.nb_point_support}
        replaced = False
        for n, v in enumerate(self.saved_results):
            if v['file_name'] == filename:
                self.saved_results[n] = d
                replaced = True
                break
        if replaced is False:
            self.saved_results.append(d)

        if False:
            self._print("To view the result file (HDF5/CXI or npz), use: silx view %s" % filename)


class CDIRunnerBL9C(CDIRunner):
    """
    Class to process a series of scans with a series of algorithms, given from the command-line
    """

    def __init__(self, argv, params, ptycho_runner_scan_class):
        super(CDIRunnerBL9C, self).__init__(argv, params, ptycho_runner_scan_class)
        self.help_text += helptext_beamline

        #ZeroMQ Context
        CONTEXT = zmq.Context()
        self.sock = CONTEXT.socket(zmq.PUB)
        for item in argv:
            if item.startswith('zmq_log_port'):
                port = item.split('=')[-1]

        self.sock.connect("tcp://localhost:"+str(port))

    def _print(self, msg, *args):
        """print msg to stdout and zmq port"""
        # print a string to stdout
        print(msg)
        if type(msg) == 'str':
            # send to zmq
            self.sock.send(msg.encode())

        for msg in args:
                # print a string to stdout
                print(msg)
                if type(msg) == str:
                    # send to zmq
                    self.sock.send(msg.encode())

    def parse_arg(self, k, v=None):
        """
        Interprets one parameter. Will
        :param k: string with the name of the parameter
        :param v: value of the parameter, or None if a keyword parameter
        :return: True if parameter could be interpreted, False otherwise
        """
        print(k, v)
        if k == 'liveplot':
            k = 'live_plot'
        if k == 'roi':
            k = 'roi_user'
        if k == 'pixel_size_data':
            k = 'pixel_size_detector'
        if k == 'support_type':
            warnings.warn("'support_type=' is deprecated, use 'support=' instead", DeprecationWarning)
            k = 'support'
        if k in ['live_plot']:
            if v is None:
                self.params[k] = True
                return True
            elif type(v) is bool:
                self.params[k] = v
                return True
            elif type(v) is str:
                if v.lower() == 'true' or v.lower() == '1':
                    self.params[k] = True
                    return True
                elif v.lower() == 'false' or v.lower() == '0':
                    self.params[k] = False
                    return True
                else:
                    self.params[k] = int(v)
                    return True
            else:
                return False
        if k in ['auto_center_resize', 'data2cxi', 'positivity', 'support_only_shrink', 'detwin', 'crop_output', 'psf']:
            if v is None:
                self.params[k] = True
                return True
            elif type(v) is bool:
                self.params[k] = v
                return True
            elif type(v) is str:
                if v.lower() == 'true' or v.lower() == '1':
                    self.params[k] = True
                    return True
                else:
                    self.params[k] = False
                    return True
            else:
                return False
        elif k in ['detector_distance', 'pixel_size_data', 'pixel_size_detector', 'wavelength',
                   'support_smooth_width_begin', 'support_smooth_width_end', 'beta',
                   'support_autocorrelation_threshold', 'iobs_saturation']:
            self.params[k] = float(v)
            return True
        elif k in ['support_threshold']:
            if ',' in v:
                self.params[k] = [float(val) for val in v.split(',')]  # a range is given
                self.params[k].sort()
            else:
                self.params[k] = float(v)  # single value
            return True
        elif k in ['verbose', 'nb_run', 'nb_run_keep', 'max_size', 'support_update_period', 'nb_raar', 'nb_hio',
                   'nb_er', 'nb_ml', 'support_smooth_width_relax_n', 'support_update_border_n', 'zmq_log_port', 'zmq_img_port']:
            self.params[k] = int(v)
            return True
        elif k in ['data', 'mask', 'support', 'object', 'rebin', 'support_threshold_method', 'save',
                   'output_format', 'support_size', 'note', 'instrument', 'sample_name', 'algorithm', 'zero_mask',
                   'free_pixel_mask', 'support_formula']:
            self.params[k] = v
            return True
        elif k in ['gpu']:
            # Allows several GPU (sub)strings to be listed
            g = v.split(',')
            # Use either a string or a list, to check if both cases are correctly processed
            if len(g) == 1:
                self.params[k] = g[0]
            else:
                self.params[k] = g
        elif k in ['support_post_expand', 'roi_user']:
            self.params[k] = eval(v)
            return True
        elif 'user_config' in k:
            # These parameters are only used to store some information about the process, and will be stored
            # in the cxi output
            self.params[k] = v
        else:
            return self.parse_arg_beamline(k, v)

    def parse_arg_beamline(self, k, v):
        """
        Parse argument in a beamline-specific way. This function only parses single arguments.
        If an argument is recognized and interpreted, the corresponding value is added to self.params

        This method should be superseded in a beamline/instrument-specific child class
        Returns:
            True if the argument is interpreted, false otherwise
        """
        if k in ['specfile', 'imgcounter', 'imgname', 'scan']:
            self.params[k] = v
            return True
        return False

    def check_params_beamline(self):
        """
        Check if self.params includes a minimal set of valid parameters, specific to a beamiline
        Returns: Nothing. Will raise an exception if necessary
        """
        print()
        if self.params['data'] is None and (self.params['specfile'] is None or self.params['scan'] is None):
            raise CDIRunnerException('No data provided. Need at least data=, or specfile= and scan=')

    def process_scans(self):
        """
        Run all the analysis on the supplied scan list

        :return: Nothing
        """
        if 'scan' in self.params:
            if isinstance(self.params['scan'], str):
                if 'range' in self.params['scan']:
                    # use range() function to specify a series of scans
                    vscan = eval(self.params['scan'])
                else:
                    # + is used to combine several scans in one, but there may still be a series of scans, e.g. '1+2,3+4'
                    vscan = self.params['scan'].split(',')
            else:
                # This could be None, the default value for self.params['scan'], to load a CXI, numpy, tiff file, etc...
                vscan = [self.params['scan']]
        else:
            vscan = [None]

        for scan in vscan:
            if scan is not None:
                self._print("\n", "#" * 100, "\n#", "\n#  CDI Scan: ", scan, "\n#\n", "#" * 100)
            try:
                self.ws = self.CDIRunnerScan(self.params, scan)
                self.ws.load_data()
                need_init_mask = True

                # Get prefix to save CXI and output file
                file_prefix = None
                if 'specfile' in self.params:
                    if self.params['specfile'] is not None:
                        self._print(self.params['specfile'])
                        file_prefix = os.path.splitext(os.path.split(self.params['specfile'])[1])[0]

                if file_prefix is None and 'data' in self.params:
                    if self.params['data'] is not None:
                        file_prefix = os.path.splitext(self.params['data'])[0]

                if file_prefix is None:
                    file_prefix = 'data'

                if isinstance(scan, str):
                    try:
                        s = int(scan)
                        file_prefix = file_prefix + '_S%04d' % s
                    except ValueError:
                        file_prefix = file_prefix + '_S%s' % scan
                elif isinstance(scan, int):
                    file_prefix = file_prefix + '_S%04d' % scan

                if self.params['data2cxi']:
                    # This will save the original data, before cropping & centering
                    file_name = file_prefix + '.cxi'
                    if os.path.isfile(file_name):
                        self._print("Data CXI file already exists, not overwriting: ", file_name)
                    else:
                        self.ws.init_mask()
                        self.ws.corr_flat_field()
                        need_init_mask = False
                        self._print("Saving data to CXI file: ", file_name)
                        save_cdi_data_cxi(file_name, self.ws.iobs, wavelength=self.params['wavelength'],
                                          detector_distance=self.params['detector_distance'],
                                          pixel_size_detector=self.params['pixel_size_detector'],
                                          sample_name=self.params['sample_name'], mask=self.ws.mask,
                                          instrument=self.params['instrument'], note=self.params['note'],
                                          process_parameters=self.params)

                self.ws.prepare_processing_unit()
                self.ws.prepare_data(init_mask=need_init_mask, corr_flat_field=need_init_mask)

                support_threshold = self.params['support_threshold']

                # Keep free pixel mask between successive runs
                free_pixel_mask = None

                for run in range(1, self.params['nb_run'] + 1):
                    # KLUDGE: modifying the support threshold in params may not be the best way ?
                    if isinstance(support_threshold, list) or isinstance(support_threshold, tuple):
                        self.params['support_threshold'] = np.random.uniform(support_threshold[0], support_threshold[1])

                    self._print("\n", "#" * 100, "\n#", "\n#  CDI Run: %g/%g\n#\n" % (run, self.params['nb_run']), "#" * 100)
                    s = time.strftime("-%Y-%m-%dT%H-%M-%S", time.localtime())
                    if self.params['nb_run'] > 1:
                        s += "_Run%04d" % run
                    self.ws.prepare_cdi(free_pixel_mask=free_pixel_mask)
                    self.ws.run(file_name=file_prefix + s)
                    free_pixel_mask = self.ws.cdi.get_free_pixel_mask()

                if self.params['nb_run'] > 1 and self.params['nb_run_keep'] is not None:
                    # Keep only some of the best runs, delete others
                    if self.params['nb_run'] > self.params['nb_run_keep']:
                        self.ws.saved_results.sort(key=lambda x: x['llk_poisson_free'])
                        for i in range(self.params['nb_run_keep'], len(self.ws.saved_results)):
                            self._print("Removing: %s" % self.ws.saved_results[i]['file_name'])
                            os.system('rm -f "%s"' % self.ws.saved_results[i]['file_name'])
                        self.ws.saved_results = self.ws.saved_results[:self.params['nb_run_keep']]
                        os.system('rm -f %s' % 'latest.cxi')
                    else:
                        self._print('CDI runner: nb_run=%d < nb_run_keep=%d !! Keeping all results' % (
                            self.params['nb_run'], self.params['nb_run_keep']))

            except Exception as ex:
                self._print(traceback.format_exc())
                self._print(self.help_text)
                self._print('\n\n Caught exception for scan %s:\n   %s    \n' % (str(scan), str(ex)))
                sys.exit(1)
