#! /opt/local/bin/python
# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2017-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr

from __future__ import division

import sys
from pynx.cdi.runner import helptext_generic, CDIRunnerException
from xicam.cdi.workflows.pynx.runner.bl9c import CDIRunnerBL9C, CDIRunnerScanBL9C, helptext_beamline, params

FILE = __file__

help_text = helptext_generic + helptext_beamline


if __name__ == '__main__':
    try:
        w = CDIRunnerBL9C(sys.argv, params, CDIRunnerScanBL9C)
        w.process_scans()
    except CDIRunnerException as ex:
        print(help_text)
        print('\n\n Caught exception: %s    \n' % (str(ex)))
        sys.exit(1)

