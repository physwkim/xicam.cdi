import numpy as np
import xrayutilities as xu
from skimage.external import tifffile

"""
ORIG
qconv = xu.experiment.QConversion(['z-', 'y+', 'x-'], ['x-','z-'], [0, 1, 0])
hxrd = xu.HXRD((0, 1, 0), (1, 0, 0), en=8000, qconv=qconv)
hxrd.Ang2Q.init_area('x-', 'z+', cch1=64, cch2=64, Nch1=128, Nch2=128, pwidth1=55e-6, pwidth2=55e-6, distance=0.907)
"""

"""
NEW

x : beam direction
z : surface normal to sample at tth=0

"""

# mu, th, chi, phi, nu, tth
# incident beam : x-axis
# surface normal : z-axis
qconv = xu.experiment.QConversion(['z-', 'y-', 'x+', 'z-'], ['z-','y-'], [1, 0, 0])
hxrd = xu.HXRD((1, 0, 0), (0, 0, 1), en=8000, qconv=qconv)
hxrd.Ang2Q.init_area('z-', 'y+', cch1=64, cch2=64, Nch1=128, Nch2=128, pwidth1=55e-6, pwidth2=55e-6, distance=0.907)

frames = tifffile.imread('191128_S534_crop_post_post.tif')
nz, ny, nx = frames.shape
iobs = np.empty((nz, ny, nx), dtype=np.float32)

for i in range(nz):
    iobs[i] = np.array(frames[i])

qx = []
qy = []
qz = []
for idx in range(nz):
    _qx, _qy, _qz=hxrd.Ang2Q.area(0, 18.807/2. + 0.005 * idx, 0, 0 , 0, 18.807)
    qx.append(_qx)
    qy.append(_qy)
    qz.append(_qz)


qx = np.array(qx)
qy = np.array(qy)
qz = np.array(qz)

gridder = xu.FuzzyGridder3D(nz , ny, nx)
gridder(qx, qy, qz, iobs)

# Save to an tif file
print("gridder data's shape = ", gridder.data.shape)
tifffile.imsave('temp6.tif', gridder.data)

