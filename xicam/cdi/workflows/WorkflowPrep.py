from xicam.cdi.workflows.preprocessPlugin import PreprocessPlugin
from xicam.core.execution.workflow import Workflow

class PreprocessWorkflow(Workflow):
    def __init__(self):
        super(PreprocessWorkflow, self).__init__('Preprocess')

        self.preprocess = PreprocessPlugin()

        self.processes = [self.preprocess]
        self.autoConnectAll()
