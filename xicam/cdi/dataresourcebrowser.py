import os
import logging
from pathlib import Path
from warnings import warn
from typing import Union, List
from urllib import parse

from silx.gui import qt

from xicam.gui.widgets.dataresourcebrowser import \
        DataResourceBrowser as _DataResourceBrowser
from xicam.gui.widgets.dataresourcebrowser import BrowserTabWidget, \
                                      BrowserTabBar, \
                                      DataBrowser as _DataBrowser, \
                                      LocalFileSystemTree as _LocalFileSystemTree

from xicam.gui.clientonlymodels.LocalFileSystemResource import \
        LocalFileSystemResourcePlugin as _LocalFileSystemResourcePlugin

from xicam.core.data import NonDBHeader, detect_mimetype, UnknownFileType, \
                            NoIngestor, choose_ingestor
from xicam.core import msg

from databroker.utils import ALL
from databroker.in_memory import BlueskyInMemoryCatalog
from databroker.core import Header

from xicam.core.msg import DEBUG, INFO, WARNING, ERROR, CRITICAL


def load_header(uris: List[Union[str, Path]] = None, uuid: str = None):
    """
    Load a document object, either from a file source or a databroker source, by uuid. If loading from a filename, the
    file will be registered in databroker.

    Parameters
    ----------
    uris
    uuid

    Returns
    -------
    NonDBHeader

    """
    from xicam.plugins import manager as pluginmanager  # must be a late import

    # ext = Path(filename).suffix[1:]
    # for cls, extensions in extension_map.items():
    #     if ext in extensions:

    # First try to see if we have a databroker ingestor, then fall-back to Xi-cam DataHandlers
    ingestor = None
    filename = str(Path(uris[0]))
    mimetype = None
    try:
        mimetype = detect_mimetype(filename)
    except UnknownFileType as e:
        pass
    else:
        pass

    try:
        ingestor = choose_ingestor(filename, mimetype)
    except NoIngestor as e:
        pass
    else:
        pass

    if ingestor:
        document = list(ingestor(uris))
        uid = document[0][1]["uid"]
        catalog = BlueskyInMemoryCatalog()
        # TODO -- change upsert signature to put start and stop as kwargs
        # TODO -- ask about more convenient way to get a BlueskyRun from a document generator
        catalog.upsert(document[0][1], document[-1][1], ingestor, [uris], {})
        return catalog[uid]

    handlercandidates = []
    ext = Path(uris[0]).suffix
    for plugin in pluginmanager.getPluginsOfCategory("DataHandlerPlugin"):
        if ext in plugin.plugin_object.DEFAULT_EXTENTIONS:
            handlercandidates.append(plugin)
    if not handlercandidates:
        return NonDBHeader({}, [], [], {})

    return NonDBHeader(**handlercandidates[0].plugin_object.ingest(uris))


class LocalFileSystemResourcePlugin(_LocalFileSystemResourcePlugin):
    def __init__(self):
        super(_LocalFileSystemResourcePlugin, self).__init__()
        self.uri = parse.urlparse(qt.QSettings().value("lastlocaldir", os.getcwd()))
        self.setResolveSymlinks(True)

        self.setRootPath(parse.urlunparse(self.uri))

    def getHeader(self, indexes):
        uris = [self.filePath(index) for index in indexes]
        return load_header(uris=uris)


class LocalFileSystemTree(_LocalFileSystemTree):
    def __init__(self):
        super(_LocalFileSystemTree, self).__init__(LocalFileSystemResourcePlugin())


class DataBrowser(_DataBrowser):
    def __init__(self, *args, **kwargs):
        super(DataBrowser, self).__init__(*args, **kwargs)

    def text_to_uri(self):
        uri = parse.urlparse(self.URILineEdit.text())
        self.browserview.model().uri = uri
        return uri


class DataResourceBrowser(_DataResourceBrowser):
    def __init__(self):
        super(_DataResourceBrowser, self).__init__()

        vbox = qt.QVBoxLayout()
        vbox.setSpacing(0)
        vbox.setContentsMargins(0, 0, 0, 0)
        self.setSizePolicy(qt.QSizePolicy.Expanding, qt.QSizePolicy.Expanding)
        self.setMinimumSize(qt.QSize(250, 400))

        self.browsertabwidget = BrowserTabWidget(self)
        self.browsertabbar = BrowserTabBar(self.browsertabwidget)
        self.browsertabbar.sigAddBrowser.connect(self.addBrowser)
        self.browsertabbar.tabCloseRequested.connect(self.closetab)

        # Add the required 'Local' browser
        self.addBrowser(DataBrowser(LocalFileSystemTree()), "Local", closable=False)
        self.browsertabbar.setCurrentIndex(0)

        vbox.addWidget(self.browsertabwidget)

        self.setLayout(vbox)
