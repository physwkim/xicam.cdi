import numpy as np

from silx.gui import qt
from silx.gui.plot.ComplexImageView import ComplexImageView as _ComplexImageView

class ComplexImageView(_ComplexImageView):
    """
    Inherited class from silx.gui.plot.ComplexImageView
    
    Display an image of complex data and allow to choose the visualization.
    :param parent: See :class:`QMainWindow`
    """

    def __init__(self, parent=None, *args, **kwargs):
        super(ComplexImageView, self).__init__(*args, **kwargs)

    def setData(self, data=None, pixelSizeFFT=None, scanArea=None, copy=True):
        """Set the complex data to display.

        :param numpy.ndarray data: 2D complex data
        :param bool copy: True (default) to copy the data,
                          False to use provided data (do not modify!).
        """
        if data is None:
            data = np.zeros((0, 0), dtype=np.complex)


        previousData = self._plotImage.getComplexData(copy=False)

        self._plotImage.setData(data, copy=copy)

        if pixelSizeFFT is not None:
            ny, nx = data.shape
            origin = (-nx / 2.* pixelSizeFFT, -ny / 2. * pixelSizeFFT )
            scale = (pixelSizeFFT, pixelSizeFFT)
            self._plotImage.setScale(scale)
            self._plotImage.setOrigin(origin)

        if previousData.shape != data.shape:
            self.getPlot().resetZoom()

        if scanArea is not None:
            x_scan_area = scanArea[0]
            y_scan_area = scanArea[1]
            self.getPlot().addCurve(x_scan_area * pixelSizeFFT,
                                    y_scan_area * pixelSizeFFT,
                                    legend='scanArea',
                                    color='black')


