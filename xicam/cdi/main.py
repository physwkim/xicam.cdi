import sys
from functools import partial

from silx.gui import qt

from xicam.core import msg

from xicam.gui.static import path
from xicam.gui.windows.settings import ConfigDialog
from xicam.gui.windows.mainwindow import XicamMainWindow as _XicamMainWindow, \
                                         pluginModeWidget
from xicam.gui.widgets.motd import MOTD
from xicam.gui.widgets import defaultstage
from xicam.gui.widgets.debugmenubar import DebuggableMenuBar

from xicam.plugins import manager as pluginmanager
from xicam.plugins.guiplugin import GUILayout
from xicam.plugins.guiplugin import PanelState

from dataresourcebrowser import DataResourceBrowser
from previewwidget import PreviewWidget

defaultstage = GUILayout(center=MOTD(), left=DataResourceBrowser(), lefttop=PreviewWidget())

class MainWindow(_XicamMainWindow):
    def __init__(self):
        super(_XicamMainWindow, self).__init__()

        # Set icon
        self.setWindowIcon(qt.QIcon(qt.QPixmap(str(path("icons/xicam.gif")))))

        # Set size and position
        self.setGeometry(0, 0, 1000, 600)
        frameGm = self.frameGeometry()
        screen = qt.QApplication.desktop().screenNumber(qt.QApplication.desktop().cursor().pos())
        centerPoint = qt.QApplication.desktop().screenGeometry(screen).center()
        frameGm.moveCenter(centerPoint)
        self.move(frameGm.topLeft())

        # Init child widgets to None
        self.topwidget = (
            self.leftwidget
        ) = (
            self.rightwidget
        ) = (
            self.bottomwidget
        ) = self.lefttopwidget = self.righttopwidget = self.leftbottomwidget = self.rightbottomwidget = None

        # Setup appearance
        self.setWindowTitle("Xi-cam")

        # Load plugins
        pluginmanager.collectPlugins()

        # Restore Settings
        self._configdialog = ConfigDialog()
        self._configdialog.restore()

        # Setup center/toolbar/statusbar/progressbar
        self.pluginmodewidget = pluginModeWidget()
        self.pluginmodewidget.sigSetStage.connect(self.setStage)
        self.pluginmodewidget.sigSetGUIPlugin.connect(self.setGUIPlugin)
        self.addToolBar(self.pluginmodewidget)
        self.setStatusBar(qt.QStatusBar(self))
        msg.progressbar = qt.QProgressBar(self)
        msg.progressbar.hide()
        msg.statusbar = self.statusBar()
        self.statusBar().addPermanentWidget(msg.progressbar)
        self.setCentralWidget(qt.QStackedWidget())
        # NOTE: CentralWidgets are force-deleted when replaced, even if the object is still referenced;
        # To avoid this, a QStackedWidget is used for the central widget.

        # Setup menubar
        menubar = DebuggableMenuBar()
        self.setMenuBar(menubar)
        file = qt.QMenu("&File", parent=menubar)
        menubar.addMenu(file)
        file.addAction("Se&ttings", self.showSettings, shortcut=qt.QKeySequence(qt.Qt.CTRL + qt.Qt.ALT + qt.Qt.Key_S))
        file.addAction("E&xit", self.close)
        help = qt.QMenu("&Help", parent=menubar)
        menubar.addMenu(help)

        # Initialize layout with first plugin
        self._currentGUIPlugin = None
        self.build_layout()

        # self._currentGUIPlugin = pluginmanager.getPluginsOfCategory("GUIPlugin")[0]
        self.populate_layout()

        # Make F key bindings
        fkeys = [
            qt.Qt.Key_F1,
            qt.Qt.Key_F2,
            qt.Qt.Key_F3,
            qt.Qt.Key_F4,
            qt.Qt.Key_F5,
            qt.Qt.Key_F6,
            qt.Qt.Key_F7,
            qt.Qt.Key_F8,
            qt.Qt.Key_F9,
            qt.Qt.Key_F10,
            qt.Qt.Key_F11,
            qt.Qt.Key_F12,
        ]
        self.Fshortcuts = [qt.QShortcut(qt.QKeySequence(key), self) for key in fkeys]
        for i in range(12):
            self.Fshortcuts[i].activated.connect(partial(self.setStage, i))


        self.readSettings()
        # Wireup default widgets
        defaultstage["left"].sigOpen.connect(self.open)
        # defaultstage["left"].sigOpen.connect(print)
        defaultstage["left"].sigPreview.connect(defaultstage["lefttop"].preview_header)


    def populate_layout(self):
        # Get current stage
        if self.currentGUIPlugin:
            stage = self.currentGUIPlugin.stage
        else:
            stage = defaultstage

        # Set center contents
        self.centralWidget().addWidget(stage.centerwidget)
        self.centralWidget().setCurrentWidget(stage.centerwidget)

        # Set visibility based on panel state and (TODO) insert default widgets when defaulted
        for position in ["top", "left", "right", "bottom", "lefttop", "righttop", "leftbottom", "rightbottom"]:
            self.populate_hidden(stage, position)
            self.populate_position(stage, position)

    def populate_hidden(self, stage, position):
        getattr(self, position + "widget").setHidden(
            (stage[position] == PanelState.Disabled)
            or (stage[position] == PanelState.Defaulted and defaultstage[position] == PanelState.Defaulted)
        )

    def populate_position(self, stage, position: str):
        if isinstance(stage[position], qt.QWidget):
            getattr(self, position + "widget").setWidget(stage[position])
        elif stage[position] == PanelState.Defaulted:
            if not defaultstage[position] == PanelState.Defaulted:
                getattr(self, position + "widget").setWidget(defaultstage[position])
        elif isinstance(stage[position], type):
            raise TypeError(
                f"A type is not acceptable value for stages. You must instance this class: {stage[position]}, {position}"
            )


