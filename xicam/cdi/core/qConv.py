from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
# Programmed by Sang-Woo Kim, PAL(POSTECH),  2017/11/17
# Filename: qConv.py

import numpy as np
import xrayutilities as xu
import os
from PIL import Image
#import matplotlib.pyplot as plt
import time

DEBUG = 0

params = {}
params['energy'] = 11570 # in eV
params['px_dir1'] = 'x-'
params['px_dir2'] = 'z-'
params['cch1'] = 212.75
params['cch2'] = 230
params['Nch1'] = 407
params['Nch2'] = 487
params['pwidth1'] = 172e-06
params['pwidth2'] = 172e-06
params['distance'] = 0.95336
params['detrot'] = 0
params['tiltazimuth'] = 0
params['tilt'] = 0
params['Nav'] = (1,1)
params['region_of_interest'] = (220,396,234,452)
params['outerangle_offset'] = 0

# Start Angles
params['i_nu'] = 0
params['i_tth'] = 10
params['i_th'] = 5
params['i_chi'] = 0
params['i_phi'] = 0

# Delta angles per step
params['d_nu'] = 0
params['d_tth'] = 0.5
params['d_th'] = 0.25
params['d_chi'] = 0
params['d_phi'] = 0

class qCon:
    """CCD image to Q space conversion class"""
    def __init__(self, energy, energyDelta, px_dir1, px_dir2, cch1, cch2, Nch1, Nch2, pwidth1, pwidth2, distance,\
                 detrot, tiltazimuth, tilt, Nav, region_of_interest, outerangle_offset, i_nu, i_tth, i_th,\
                 i_chi, i_phi, d_nu, d_tth, d_th, d_chi, d_phi):

        self.energy = energy + energyDelta
        self.pwidth1 = pwidth1
        self.pwidth2 = pwidth2

        self.nu=i_nu
        self.tth=i_tth
        self.th=i_th
        self.chi=i_chi
        self.phi=i_phi

        self.i_nu=i_nu
        self.i_tth=i_tth
        self.i_th=i_th
        self.i_chi=i_chi
        self.i_phi=i_phi

        self.d_nu = d_nu
        self.d_tth = d_tth
        self.d_th = d_th
        self.d_chi = d_chi
        self.d_phi = d_phi

        self.count=0

        self.xarr=[]
        self.yarr=[]
        self.qdata=[]

        self.roi=region_of_interest
        #self.sumNum=self.roi[3]-self.roi[2]+1
        #print("roi[0],roi[1],roi[2],roi[3] = ",self.roi)
        #print "self.sumNum=",self.sumNum

        self.startPix=0
        self.finishPix=0

        self.outerangle_offset = outerangle_offset

        self.sAng=np.double(i_tth)
        self.fAng=np.double(60)
        self.stepAng=np.double(0.005)


        self.xintp=[]
        self.yintp=[]

        #diffractometer & axis info
        self.qconv = xu.experiment.QConversion(['z-', 'y+', 'x-'], ['x-','z-'], [0, 1, 0])
        self.hxrd = xu.HXRD((1, 0, 0), (0, 0, 1), en=self.energy, qconv=self.qconv)
        self.hxrd.Ang2Q.init_area(px_dir1, px_dir2, cch1=cch1, cch2=cch2, Nch1=Nch1, Nch2=Nch2,\
                pwidth1=pwidth1, pwidth2=pwidth2, distance=distance, detrot=detrot, tiltazimuth=tiltazimuth,\
                tilt=tilt, Nav=Nav, roi=self.roi)

    def lineExtract(self, path, stepAng=0.005, qMax=0, qMin=0, qX_Max=0, qX_Min=0, _type=0, center=1, average=False):
        """Extract 1D data from Qtot vs (Qx or Qz) 2d data"""
        # type = 0 :convert q to tth, type = 1 : return q
        self.xarr=np.array([])
        self.yarr=np.array([])

        self.rawData=[]
        self.count=0

        self.stepAng = stepAng   # the sampling step size in degree

        for item in np.sort(os.listdir(path)):
            item=path+'/'+item
            imgArr=np.asarray(Image.open(item))
            reduced_img = imgArr[self.roi[0]:self.roi[1],self.roi[2]:self.roi[3]]

            # qx : vertical, qy : along beam direction , qz : normal to qx,qy plane
            qx,qy,qz=self.hxrd.Ang2Q.area(self.th,self.chi,self.phi,self.nu,self.tth,\
                    delta=(0,0,0,self.outerangle_offset,0))
            #print("len(qx),len(qx[0])",len(qx),len(qx[0]))

            qtot=self.qTotal(qx,qy,qz)

            # TODO gridding should be modified when rotating CCD
            gridder = xu.FuzzyGridder2D(self.roi[1]-self.roi[0],self.roi[3]-self.roi[2])

            # ORIG gridder = xu.FuzzyGridder2D(self.roi[3]-self.roi[2]+1,self.roi[1]-self.roi[0]+1)
            gridder(qz,qtot,reduced_img)

            #Extract pixel information of the first image to remove invalid region
            if self.count==0:
                if DEBUG:
                    print("gridder.xaxis = ", gridder.xaxis)
                    print("np.where(gridder.xaxis >= qX_Min) = ", np.where(gridder.xaxis >= qX_Min))

                self.startPix=(np.where(gridder.yaxis >= qMin))[0][0]
                self.finishPix=(np.where(gridder.yaxis <= qMax))[0][-1]
                self.leftPix=(np.where(gridder.xaxis >= qX_Min))[0][0]
                self.rightPix=(np.where(gridder.xaxis <= qX_Max))[0][-1]

                if DEBUG:
                    print("leftPix = ", self.leftPix, "rightPix = ", self.rightPix)
                    print("startPix=",self.startPix,"finishPix=",self.finishPix)

            #Qplot plot
            X,Y=np.meshgrid(gridder.xaxis, gridder.yaxis)

            print("count=%d, tth=%f"%(self.count,self.tth))

            x,y=self.boxSum(gridder.xaxis, gridder.yaxis, gridder.data, _type, center)

            # save Raw data for calibration
            self.rawData.append([x,y])

            self.xarr=np.append(self.xarr,x)
            self.yarr=np.append(self.yarr,y)

            self.nu += self.d_nu
            self.tth += self.d_tth
            self.th += self.d_th
            self.chi += self.d_chi
            self.phi += self.d_phi

            self.count+=1

        ## sort xarr, yarr
        temp=np.array(list(zip(self.xarr,self.yarr)))
        temp_sort=temp[np.argsort(temp[:,0])]

        self.xarr=temp_sort[:,0]
        self.yarr=temp_sort[:,1]


        ## Calculate interpolated data
        self.sAng=np.double(np.min(self.xarr))
        self.fAng=np.double(np.max(self.xarr))
        self.xintp=np.linspace(self.sAng,self.fAng,(self.fAng-self.sAng)/self.stepAng)
        #print(np.asarray(self.xarr).flatten())


        self.yintp=np.interp(self.xintp,self.xarr,self.yarr)

        print(self.yarr)
        print(self.sumNum)

        if average:
            self.yarr = self.yarr/float(self.sumNum)
            self.yintp = self.yintp/float(self.sumNum)

        return self.xarr, self.yarr, self.xintp, self.yintp, self.rawData



    def lineData(self, path, plot=False, plotQ=False, qMax=0, qMin=0, _type=0, center=1,Avg=0, scale=1, save=0, outFile='line.dat'):
        """Extract 1D data from Qtot vs Qx 2d data"""
        self.xarr=np.array([])
        self.yarr=np.array([])
        self.count=0


        for item in np.sort(os.listdir(path)):
            item=path+'/'+item
            imgArr=np.asarray(Image.open(item))
            reduced_img = imgArr[self.roi[0]:self.roi[1],self.roi[2]:self.roi[3]]

            # qx : vertical, qy : along beam direction , qz : normal to qx,qy plane
            qx,qy,qz=self.hxrd.Ang2Q.area(self.th,self.chi,self.phi,self.nu,self.tth,delta=(0,0,0,self.outerangle_offset,0))
            #print("len(qx),len(qx[0])",len(qx),len(qx[0]))

            qtot=self.qTotal(qx,qy,qz)

            # TODO gridding should be modified when rotating CCD
            gridder = xu.FuzzyGridder2D(self.roi[1]-self.roi[0],self.roi[3]-self.roi[2])

            # ORIG gridder = xu.FuzzyGridder2D(self.roi[3]-self.roi[2]+1,self.roi[1]-self.roi[0]+1)
            gridder(qz,qtot,reduced_img)

            # Extract pixel information of the first image
            if self.count==0:
                self.startPix=(np.where(gridder.yaxis >= qMin))[0][0]
                self.finishPix=(np.where(gridder.yaxis <= qMax))[0][-1]
                print("startPix=",self.startPix,"finishPix=",self.finishPix)

            #Qplot plot
            X,Y=np.meshgrid(gridder.xaxis,gridder.yaxis)

            print("count=%d, tth=%f"%(self.count,self.tth))
            if plotQ:
                plt.clf()
                imgplot=plt.contourf(X,Y,gridder.data.T,8,alpha=0.75,cmap='jet')
                time.sleep(0.1)
                plt.pause(1e-6)
                temp=raw_input("Press Enter to Continue: ")


            x,y=self.boxSum(gridder.xaxis, gridder.yaxis, gridder.data, _type, center)

            self.xarr=np.append(self.xarr,x)
            self.yarr=np.append(self.yarr,y)

            self.nu += self.d_nu
            self.tth += self.d_tth
            self.th += self.d_th
            self.chi += self.d_chi
            self.phi += self.d_phi

            self.count+=1

        ## sort xarr, yarr
        temp=np.array(list(zip(self.xarr,self.yarr)))
        temp_sort=temp[np.argsort(temp[:,0])]
        self.xarr=temp_sort[:,0]
        self.yarr=temp_sort[:,1]

        ## Calculate interpolated data
        self.sAng=np.double(np.min(self.xarr))
        self.fAng=np.double(np.max(self.xarr))
        self.xintp=np.linspace(self.sAng,self.fAng,(self.fAng-self.sAng)/self.stepAng)
        #print(np.asarray(self.xarr).flatten())
        self.yintp=np.interp(self.xintp,self.xarr,self.yarr)*scale

        if Avg:
            self.Average(self.sumNum)

        if save:
            if len(self.xintp) > 0:
                np.savetxt(outFile,list(zip(self.xintp,self.yintp)))
            else:
                print("Data is not exits")

        if plot:
            ### plot reference data
            #xref,yref=np.loadtxt('ccdscan75A.dat').T
            #plt.plot(xref,yref,'b-',label='tth scan with roi')

            ## for raw data plot
            plt.plot(self.xarr,self.yarr*scale,'go',label='raw_data')

            ## plot interpolated data
            plt.plot(self.xintp,self.yintp,'r-',label='tth scan with pilatus')
            plt.legend()
            plt.show()




    def boxSum(self, grid_x, grid_y, grid_data, _type=0, center=1):
        # type = 0 : convert to tth, type = 1 : q
        if center==0:
            # extract line data around absolute minimum q value
            temp=np.min(np.abs(grid_x))
            index_0 = np.where(grid_x == temp if temp in grid_x else temp*-1. )
            index_0 = int(index_0[0])
        else:
            # extract line data around center position of Qx
            mid=int(len(grid_x)/2)
            index_0 = np.where(grid_x == grid_x[mid])
            index_0 = int(index_0[0])

        #print("index num : ",index_0)

        sumNum=int(len(grid_x))
        self.sumNum = sumNum
        #print(sumNum)
        if sumNum%2 == 1:
            index_rel=(sumNum-1)/2.
            # sum over horizontal array
            if (index_0-index_rel) < self.leftPix:
                _left = self.leftPix
            else:
                _left = index_0-index_rel

            if (index_0+index_rel) > self.rightPix:
                _right = self.rightPix
            else:
                _right = index_0+index_rel+1

            intensity= np.sum(grid_data.T[:,int(_left):int(_right)],\
                    axis=1).flatten()[self.startPix:self.finishPix]
            qz_temp = grid_y[self.startPix:self.finishPix]
            #print("len grid_y", len(qz_temp))

        else:
            index_rel=(sumNum)/2.

            if (index_0-index_rel) < self.leftPix:
                _left = self.leftPix
            else:
                _left = index_0-index_rel

            if (index_0+index_rel) > self.rightPix:
                _right = self.rightPix
            else:
                _right = index_0+index_rel

            # sum over horizontal array
            intensity= np.sum(grid_data.T[:,int(_left):int(_right)],\
                    axis=1).flatten()[self.startPix:self.finishPix]
            qz_temp = grid_y[self.startPix:self.finishPix]
            #print("len grid_y", len(qz_temp))

        if _type == 0:
            x = 2*np.arcsin(12.39842/self.energy*1000*qz_temp/4.0/np.pi)*180./np.pi      # q to tth angle conversion
        else :
            x = qz_temp

        return x,intensity


    def Average(self,nSum):
        if len(self.xintp) > 0:
            self.yintp/=nSum
        else:
            print("Data is not exits")

    #def qRange(self,qx):
    #    qmax=qx[:,0].max()
    #    print("qmax_init",qmax)
    #    qmin=qx[:,0].min()
    #    print("qmin_init",qmin)

    #    for i in range(1,len(qx[0])):
    #        qmax_temp=qx[:,i].max()
    #        qmin_temp=qx[:,i].min()
    #        if qmax_temp < qmax:
    #            qmax=qmax_temp
    #        if qmin_temp > qmin:
    #            qmin=qmin_temp
    #    return qmax,qmin


    def qTotal(self,qx,qy,qz):
        qtot=np.zeros((len(qx),len(qx[0])))
        for i in range(len(qx)):
            for j in range(len(qx[0])):
                qtot[i,j]=np.sqrt(qx[i,j]**2+qy[i,j]**2+qz[i,j]**2)
        return qtot

    def qXY(self,qx,qy):
        qxy=np.zeros((len(qx),len(qx[0])))
        for i in range(len(qx)):
            for j in range(len(qx[0])):
                if qx[i,j]>0:
                    qxy[i,j]=np.sqrt(qx[i,j]**2+qy[i,j]**2)
                else:
                    qxy[i,j]=-1.*np.sqrt(qx[i,j]**2+qy[i,j]**2)
        return qxy

    def QSave(self,path,imgNum,_type=0,grid=0,outFile='qdata.dat'):
        """Save Selected Data : Qtotal vs Qx(type = 0), Qz vs Qxy(type = 1)"""
        item = np.sort(os.listdir(path))[imgNum]
        item=path+'/'+item
        imgArr=np.asarray(Image.open(item))
        reduced_img = imgArr[self.roi[0]:self.roi[1],self.roi[2]:self.roi[3]]

        imgDataSave=[]

        # set goniometer angles
        self.nu += self.d_nu*float(imgNum)
        self.tth += self.d_tth*float(imgNum)
        self.th += self.d_th*float(imgNum)
        self.chi += self.d_chi*float(imgNum)
        self.phi += self.d_phi*float(imgNum)

        if _type == 1:
            # qx : vertical, qy : along beam direction , qz : normal to qx,qy plane
            qx,qy,qz=self.hxrd.Ang2Q.area(self.th,self.chi,self.phi,self.nu,self.tth,delta=(0,0,0,self.outerangle_offset,0))
            qzy=self.qXY(qz,qy)
            # TODO gridding should be modified when rotating CCD
            gridder = xu.FuzzyGridder2D(self.roi[1]-self.roi[0],self.roi[3]-self.roi[2])

            # ORIG gridder = xu.FuzzyGridder2D(self.roi[3]-self.roi[2]+1,self.roi[1]-self.roi[0]+1)
            gridder(qzy,qx,reduced_img)

            tempX=gridder.xaxis
            tempY=gridder.yaxis
            tempData=gridder.data
            if grid == False:
                for i in range(len(qx)):
                    for j in range(len(qx[0])):
                        imgDataSave.append([qzy[i,j],qx[i,j],reduced_img[i,j]])
            elif grid == True:
                for i in range(len(tempData)):
                    for j in range(len(tempData[0])):
                        imgDataSave.append([tempX[i],tempY[j],tempData[i,j]])


        else:
            # qx : vertical, qy : along beam direction , qz : normal to qx,qy plane
            qx,qy,qz=self.hxrd.Ang2Q.area(self.th,self.chi,self.phi,self.nu,self.tth,delta=(0,0,0,self.outerangle_offset,0))
            qtot=self.qTotal(qx,qy,qz)
            # ORIG gridder = xu.FuzzyGridder2D(self.roi[3]-self.roi[2]+1,self.roi[1]-self.roi[0]+1)
            # TODO gridding should be modified when rotating CCD
            gridder = xu.FuzzyGridder2D(self.roi[1]-self.roi[0],self.roi[3]-self.roi[2])
            gridder(qz,qtot,reduced_img)

            tempX=gridder.xaxis
            tempY=gridder.yaxis
            tempData=gridder.data

            if grid == False:
                for i in range(len(qx)):
                    for j in range(len(qx[0])):
                        imgDataSave.append([qz[i,j],qtot[i,j],reduced_img[i,j]])
            elif grid == True:
                for i in range(len(tempData)):
                    for j in range(len(tempData[0])):
                        imgDataSave.append([tempX[i],tempY[j],tempData[i,j]])

        np.savetxt(outFile,imgDataSave)
        print("Data Saved!")
        #Temporary plot
        X,Y=np.meshgrid(gridder.xaxis,gridder.yaxis)

        if True:
            plt.clf()
            plt.title('Image'+str(imgNum)+', tth ='+str(self.tth))
            imgplot=plt.contourf(X,Y,gridder.data.T,8,alpha=0.75,cmap='jet')
            plt.show()
            time.sleep(0.1)
            plt.pause(1e-6)


    def imgToQ(self,path,imgNum,_type=0,grid=0,outFile='qdata.dat'):
        """Save Selected Data : Qtotal vs Qx(type = 0), Qx vs Qyz(type = 1)"""
        item = np.sort(os.listdir(path))[imgNum]
        item=path+'/'+item
        imgArr=np.asarray(Image.open(item))
        reduced_img = imgArr[self.roi[0]:self.roi[1],self.roi[2]:self.roi[3]]
       
        # print("reduced_img[:,0]=",np.array(reduced_img)[:,0])
        # print("reduced_img[0,:]=",np.array(reduced_img)[0,:])
        # print("len(reduced_img[:,0]=",len(np.array(reduced_img)[:,0]))
        # print("len(reduced_img[0,:]=",len(np.array(reduced_img)[0,:]))
        # print("roi[1]-roi[0]=",self.roi[1]-self.roi[0])
        # print("roi[3]-roi[2]=",self.roi[3]-self.roi[2])
        # print("roi[0],roi[1],roi[2],roi[3] = ",self.roi[0],self.roi[1],self.roi[2],self.roi[3])

        QimgData_raw=[]
        QimgData_grid=[]

        self.nu = self.i_nu
        self.tth = self.i_tth
        self.th = self.i_th
        self.chi = self.i_chi
        self.phi = self.i_phi

        # set goniometer angles
        self.nu += self.d_nu*float(imgNum)
        self.tth += self.d_tth*float(imgNum)
        self.th += self.d_th*float(imgNum)
        self.chi += self.d_chi*float(imgNum)
        self.phi += self.d_phi*float(imgNum)

        if _type == 1:
            # qx : vertical, qy : along beam direction , qz : normal to qx,qy plane
            qx,qy,qz=self.hxrd.Ang2Q.area(self.th,self.chi,self.phi,self.nu,self.tth,delta=(0,0,0,self.outerangle_offset,0))
            qzy=self.qXY(qz,qy)
            # TODO gridding should be modified when rotating CCD
            gridder = xu.FuzzyGridder2D(self.roi[1]-self.roi[0],self.roi[3]-self.roi[2])

            #Orig gridder = xu.FuzzyGridder2D(self.roi[3]-self.roi[2]+1,self.roi[1]-self.roi[0]+1)
            gridder(qzy,qx,reduced_img)

            tempX=gridder.xaxis
            tempY=gridder.yaxis
            tempData=gridder.data

            for i in range(len(qx)):
                for j in range(len(qx[0])):
                    QimgData_raw.append([qzy[i,j],qx[i,j],reduced_img[i,j]])

            for i in range(len(tempData)):
                for j in range(len(tempData[0])):
                    QimgData_grid.append([tempX[i],tempY[j],tempData[i,j]])

        elif _type == 2:
            # qx : vertical, qy : along beam direction , qz : normal to qx,qy plane
            qx,qy,qz=self.hxrd.Ang2Q.area(self.th,self.chi,self.phi,self.nu,self.tth,delta=(0,0,0,self.outerangle_offset,0))
            qtot=self.qTotal(qx,qy,qz)

            # polar angle
            sign=qx*0+1 
            sign[np.logical_or(np.logical_and(qy<0, qz<0), (qy+qz<0))]=-1
            #print("qx min = ",np.min(qx),"qx max = " , np.max(qx))
            qr= np.arccos(qx/qtot)*sign
            #print("qx/qtot min = ",np.min(qx/qtot),"qx/qtot max = " , np.max(qx/qtot))
            #print("qr min = ",np.min(qr),"qr max = " , np.max(qr))
            # ORIG gridder = xu.FuzzyGridder2D(self.roi[3]-self.roi[2]+1,self.roi[1]-self.roi[0]+1)

            # TODO gridding should be modified when rotating CCD
            gridder = xu.FuzzyGridder2D(self.roi[1]-self.roi[0],self.roi[3]-self.roi[2])

            gridder(qtot,qr,reduced_img)

            tempX=gridder.xaxis
            tempY=gridder.yaxis
            tempData=gridder.data

            for i in range(len(qx)):
                for j in range(len(qx[0])):
                    QimgData_raw.append([qtot[i,j],qr[i,j],reduced_img[i,j]])

            for i in range(len(tempData)):
                for j in range(len(tempData[0])):
                    QimgData_grid.append([tempX[i],tempY[j],tempData[i,j]])

        elif _type == 3:
            # qx : vertical, qy : along beam direction , qz : normal to qx,qy plane
            qx,qy,qz=self.hxrd.Ang2Q.area(self.th,self.chi,self.phi,self.nu,self.tth,delta=(0,0,0,self.outerangle_offset,0))
            qtot=self.qTotal(qx,qy,qz)

            # polar angle
            sign=qx*0+1 
            sign[np.logical_or(np.logical_and(qy<0, qz<0), (qy+qz<0))]=-1
            qr= np.arccos(qx/qtot)*sign

            # qtot to tth
            tth = 2*np.arcsin(12.39842/self.energy*1000*qtot/4.0/np.pi)*180./np.pi      # q to tth angle conversion

            # TODO gridding should be modified when rotating CCD
            gridder = xu.FuzzyGridder2D(self.roi[1]-self.roi[0],self.roi[3]-self.roi[2])


            #gridder = xu.FuzzyGridder2D(self.roi[3]-self.roi[2]+1,self.roi[1]-self.roi[0]+1)
            gridder(tth,qr,reduced_img)

            tempX=gridder.xaxis
            tempY=gridder.yaxis
            tempData=gridder.data

            for i in range(len(qx)):
                for j in range(len(qx[0])):
                    QimgData_raw.append([qtot[i,j],qr[i,j],reduced_img[i,j]])

            for i in range(len(tempData)):
                for j in range(len(tempData[0])):
                    QimgData_grid.append([tempX[i],tempY[j],tempData[i,j]])


        else:
            # qx : vertical, qy : along beam direction , qz : normal to qx,qy plane
            qx,qy,qz=self.hxrd.Ang2Q.area(self.th,self.chi,self.phi,self.nu,self.tth,delta=(0,0,0,self.outerangle_offset,0))
            qtot=self.qTotal(qx,qy,qz)

            """
            print("qtot[:,0] = ", qtot[:,0])
            print("qtot[0,:] = ", qtot[0,:])

            _prev = None
            _dir_prev = None
            _rotate = False     # swap of roi is necessary if True

            for item in qtot[:,0]:
                if _prev is None:
                    _prev = item
                    continue
                _dir = (item - _prev) >0
                print("\n")
                print("item =" , item)
                print("_dir =" , _dir)
                print("_dir_prev =" , _dir_prev)
                _prev = item

                if _dir_prev is None:
                    _dir_prev = _dir
                    continue
                
                if _dir != _dir_prev:
                    _rotate = True
                    break

                _dir_prev = _dir

            if _rotate:
                gridder = xu.FuzzyGridder2D(self.roi[1]-self.roi[0],self.roi[3]-self.roi[2])
            else:
                gridder = xu.FuzzyGridder2D(self.roi[3]-self.roi[2],self.roi[1]-self.roi[0])
            """

            # TODO gridding should be modified when rotating CCD
            gridder = xu.FuzzyGridder2D(self.roi[1]-self.roi[0],self.roi[3]-self.roi[2])
            gridder(qz,qtot,reduced_img)

            tempX=gridder.xaxis
            tempY=gridder.yaxis
            tempData=gridder.data

            for i in range(len(qx)):
                for j in range(len(qx[0])):
                    QimgData_raw.append([qz[i,j],qtot[i,j],reduced_img[i,j]])

            for i in range(len(tempData)):
                for j in range(len(tempData[0])):
                    QimgData_grid.append([tempX[i],tempY[j],tempData[i,j]])


        X, Y = np.meshgrid(gridder.xaxis, gridder.yaxis)

        return X, Y, gridder.data, QimgData_raw, QimgData_grid

    def QView(self,path,_type=0):
        """Select Display : Qtotal vs Qx(type = 0), Qz vs Qxy(type = 1)"""
        self.count=0
        for item in np.sort(os.listdir(path)):
            item=path+'/'+item
            imgArr=np.asarray(Image.open(item))
            reduced_img = imgArr[self.roi[0]:self.roi[1],self.roi[2]:self.roi[3]]

            if _type == 1:
                # qx : vertical, qy : along beam direction , qz : normal to qx,qy plane
                qx,qy,qz=self.hxrd.Ang2Q.area(self.th,self.chi,self.phi,self.nu,self.tth,delta=(0,0,0,self.outerangle_offset,0))
                qzy=self.qXY(qz,qy)

                # TODO gridding should be modified when rotating CCD
                gridder = xu.FuzzyGridder2D(self.roi[1]-self.roi[0],self.roi[3]-self.roi[2])

                # ORIG gridder = xu.FuzzyGridder2D(self.roi[3]-self.roi[2]+1,self.roi[1]-self.roi[0]+1)
                gridder(qzy,qx,reduced_img)
            else:
                # qx : vertical, qy : along beam direction , qz : normal to qx,qy plane
                qx,qy,qz=self.hxrd.Ang2Q.area(self.th,self.chi,self.phi,self.nu,self.tth,delta=(0,0,0,self.outerangle_offset,0))
                qtot=self.qTotal(qx,qy,qz)

                # TODO gridding should be modified when rotating CCD
                gridder = xu.FuzzyGridder2D(self.roi[1]-self.roi[0],self.roi[3]-self.roi[2])

                # ORIG gridder = xu.FuzzyGridder2D(self.roi[3]-self.roi[2]+1,self.roi[1]-self.roi[0]+1)
                gridder(qz,qtot,reduced_img)

            #Temporary plot
            #print(len(gridder.xaxis))
            X,Y=np.meshgrid(gridder.xaxis,gridder.yaxis)
            #print(len(gridder.data),len(gridder.data[0]))

            print("count=%d, tth=%f"%(self.count,self.tth))
            if True:
                plt.clf()
                plt.title('Image'+str(self.count)+', tth ='+str(self.tth))
                imgplot=plt.contourf(X,Y,gridder.data.T,8,alpha=0.75,cmap='jet')
                time.sleep(0.1)
                plt.pause(1e-6)
                temp=raw_input("Press Enter to Continue: ")
            self.nu += self.d_nu
            self.tth += self.d_tth
            self.th += self.d_th
            self.chi += self.d_chi
            self.phi += self.d_phi

            self.count+=1

    def imView(self,path,vmax=1000):
        """Display Images in the folder"""
        count=0
        for item in np.sort(os.listdir(path)):
            item=path+'/'+item
            img=Image.open(item)
            plt.title('Image'+str(count))
            imgplot=plt.imshow(img,vmax=vmax)
            plt.pause(1e-6)
            print("Image%d"%(count))
            count+=1
            temp=raw_input("Press Enter to Continue: ")

    def calibration(self, nu_Range, tth_Range, detaxis, priBeam, tthPath, nuPath, start, fix, energy):

        ang1, ang2 = self.makeCalArr(nu_Range, tth_Range)

        images = []

        for item in np.sort(os.listdir(nuPath)):
            item = nuPath + '/' + item
            im = np.asarray(Image.open(item))
            images.append(im)

        for item in np.sort(os.listdir(tthPath)):
            item = tthPath + '/' + item
            im = np.asarray(Image.open(item))
            images.append(im)

        param, eps = xu.analysis.sample_align.area_detector_calib(ang1, ang2, images, detaxis, priBeam,start=start,fix=fix,\
                                                                  wl=xu.en2lam(energy), nwindow=50, debug=False, plot=False)

        return param


    def makeCalArr(self, nu_Range, tth_Range):
        if len(nu_Range) == 3:
            nu_val = np.arange(nu_Range[0], nu_Range[1] + nu_Range[2], nu_Range[2])
        elif len(nu_Range) == 4:
            ex = nu_Range[3].split('-')
            nu_val = np.arange(nu_Range[0], nu_Range[1] + nu_Range[2], nu_Range[2])
            if len(ex) == 1:
                nu_val = np.delete(nu_val, int(ex[0]))
            if len(ex) == 2:
                index = range(int(ex[0]), int(ex[1]) + 1)
                nu_val = np.delete(nu_val, index)

        if len(tth_Range) == 3:
            tth_val = np.arange(tth_Range[0], tth_Range[1] + tth_Range[2], tth_Range[2])
        elif len(tth_Range) == 4:
            ex = tth_Range[3].split('-')
            tth_val = np.arange(tth_Range[0], tth_Range[1] + tth_Range[2], tth_Range[2])
            if len(ex) == 1:
                tth_val = np.delete(tth_val, int(ex[0]))
            if len(ex) == 2:
                index = range(int(ex[0]), int(ex[1]) + 1)
                tth_val = np.delete(tth_val, index)
        cnt_nu = len(nu_val)
        cnt_tth = len(tth_val)
        nu_val = np.append(nu_val, np.zeros(cnt_tth))
        tth_val = np.append(np.zeros(cnt_nu), tth_val)
        # print(len(nu_val))
        # print(len(tth_val))
        return nu_val, tth_val



path = "./S073_sample_powderscan1"
#path = "./S009_tth"
#path = "./S010_nu"



energy = 11570 # in eV

# Detector parameters
params = dict(energy = 11570,\
              px_dir1='x-', \
              px_dir2='z-', \
              cch1=212.75, \
              cch2=230, \
              Nch1=407, \
              Nch2=487, \
              pwidth1=172e-06, \
              pwidth2=172e-06, \
              distance = 0.95336, \
              detrot=0, \
              tiltazimuth=0, \
              tilt=0., \
              Nav=(1,1), \
              #region_of_interest=(0,190,130,345), \
              #region_of_interest=(0,150,233,240), \
              #region_of_interest=(220,400,233,240), \
              region_of_interest=(220,400,200,450), \
              #region_of_interest=(0,407,0,487), \
              #region_of_interest=(220,400,100,482), \
              outerangle_offset=0., \
              # Start Angles
              i_nu = 0, \
              i_tth = 10, \
              i_th = 5, \
              i_chi = 0, \
              i_phi = 0, \
              # Delta angles per step
              d_nu = 0, \
              d_tth = 0.5, \
              d_th = 0.25, \
              d_chi = 0, \
              d_phi = 0)

if __name__ == '__main__':
    a=qCon(**params)

    ## plot : Display 1D line result
    ## plotQ : Display step by step Q image
    ## qMax : Only first Data's Maximum value of Qtotal to be converted to line
    ## qMin : Only first Data's Minimum value of Qtotal to be converted to line
    ## Avg = 0(no average) , Avg = 1 (do average)
    ## scale : intensity = intensity * scale
    ## save : save the 1D data(save =1)
    ## outFile : Output file name of 1D line data

    a.lineData(path, plot=True, plotQ=False, qMax=1.068, qMin=0.875, Avg=0, scale=1,save=True, outFile='line.dat')

    ## type=0 : Qtotal vs Qx, type=1 : Qz vs Qxy
    #a.QView(path,type=0)

    ## Save data points, type=0 : Qtotal vs Qx
    ##                 , type=1 : Qz vs Qxy
    ## grid=True : save gridded data
    ## grid=False : save raw data
    ## outFile : Output file name
    #a.QSave(path,imgNum=30,type=0,grid=True,outFile='qtot.dat')

    #a.imView(path,vmax=1000)
