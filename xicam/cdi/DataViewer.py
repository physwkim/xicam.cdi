
# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2019 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/
"""This module defines a widget designed to display data using the most adapted
view from the ones provided by silx.
"""

__authors__ = ["V. Valls"]
__license__ = "MIT"
__date__ = "12/02/2019"

import logging

import h5py

from silx.gui.data.DataViewer import DataViewer as _DataViewer
from silx.gui.data.DataViews import IMAGE_MODE, COMPLEX_IMAGE_MODE
from silx.gui.data import DataViews

from xicam.cdi.DataViews import _ImageView
from xicam.cdi.DataViews import _ComplexImageView

_logger = logging.getLogger(__name__)

class DataViewer(_DataViewer):
    """ Inherited from silx.gui.data.DataViewer """

    def __init__(self, parent=None, *args, **kwargs):
        super(DataViewer, self).__init__(parent, *args, **kwargs)
        self.__pixelSizeFFT = None
    
    def createDefaultViews(self, parent=None):
        """Create and returns available views which can be displayed by default
        by the data viewer. It is called internally by the widget. It can be
        overwriten to provide a different set of viewers.

        :param QWidget parent: QWidget parent of the views
        :rtype: List[silx.gui.data.DataViews.DataView]
        """
        viewClasses = [
            DataViews._EmptyView,
            DataViews._Hdf5View,
            DataViews._NXdataView,
            DataViews._Plot1dView,
            _ImageView,
            # _ComplexImageView,
            DataViews._Plot3dView,
            DataViews._RawView,
            DataViews._StackView,
        ]
        views = []
        for viewClass in viewClasses:
            try:
                view = viewClass(parent)
                views.append(view)
            except Exception:
                _logger.warning("%s instantiation failed. View is ignored" % viewClass.__name__)
                _logger.debug("Backtrace", exc_info=True)

        return views

    def setData(self, data):
        """Set the data to view.

        It mostly can be a h5py.Dataset or a numpy.ndarray. Other kind of
        objects will be displayed as text rendering.

        :param numpy.ndarray data: The data.
        """
        self.__data = data
        if type(data) == h5py._hl.files.File:
            # Pixel size in fft space in um unit
            try:
                self.__pixelSizeFFT = data['/entry_1/object/x_pixel_size'][()] * 1E6
            except:
                # Pixel size is not exist
                self.__pixelSizeFFT = None

            try:
                x_scan_area = data['/entry_1/data_1/x_scan_area']
                y_scan_area = data['/entry_1/data_1/y_scan_area']
                self.scan_area = (x_scan_area, y_scan_area)
            except:
                self.scan_area = None

        self._invalidateInfo()
        self.__displayedData = None
        self.__updateView()
        self.__updateNumpySelectionAxis()
        self.__updateDataInView()
        self.dataChanged.emit()

    def __setDataInView(self):
        modeID = self.displayMode()
        if modeID == IMAGE_MODE or modeID == COMPLEX_IMAGE_MODE:
            self.__currentView.setData(self.__displayedData, pixelSizeFFT=self.__pixelSizeFFT, scanArea=self.scan_area)
        else:
            self.__currentView.setData(self.__displayedData)
