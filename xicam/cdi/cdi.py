import os
import signal
import inspect
import logging
import zmq
import numpy as np
import h5py

from qtpy.QtCore import *
from qtpy.QtGui import *
from qtpy.QtWidgets import *

import pyqtgraph as pg
from pyqtgraph.parametertree import Parameter, ParameterTree

from xicam.core import msg, threads
from xicam.core.msg import ERROR
from xicam.plugins import GUIPlugin, GUILayout
from xicam.plugins.guiplugin import PanelState

from silx.gui import qt
from silx.gui.plot import PlotWindow
from silx.app.view.ApplicationContext import ApplicationContext

from xicam.cdi.preprocessing import PreprocessingPanel
from xicam.cdi.reconstructionPanel import ReconstructionPanel
from xicam.cdi.reconstructionShow import DisplayRecon
from xicam.cdi.hdf5TreeView import hdf5TreeView

from xicam.cdi.workflows.WorkflowPrep import PreprocessWorkflow
from xicam.cdi.workflows.WorkflowRecon import ReconWorkflow

from xicam.cdi.DataPanel import DataPanel

# ZeroMQ Context
CONTEXT = zmq.Context()

def send_array(socket, name, A, flags=0, copy=True, track=False):
    """send a numpy array with metadata"""
    md = dict(
            name  = name,
            dtype = str(A.dtype),
            shape = A.shape,
            )
    socket.send_json(md, flags|zmq.SNDMORE)
    return socket.send(A, flags, copy=copy, track=track)


def recv_array(socket, flags=0, copy=True, track=False):
    """recv a numpy array"""
    md = socket.recv_json(flags=flags)
    msg = socket.recv(flags=flags, copy=copy, track=track)
    buf = memoryview(msg)
    name = md['name']
    A = np.frombuffer(buf, dtype=md['dtype'])
    return name, A.reshape(md['shape'])


class CDIPlugin(GUIPlugin):
    name = 'CDI'

    def __init__(self):
        self.reconStatus = None

        # ZMQ LOG port
        self.zmqLogPort = 5001
        self.zmqImgPort = 5101

        # Data model
        self.headermodel = QStandardItemModel()
        self.selectionmodel = QItemSelectionModel(self.headermodel)

        # Pixel Size in Fourier space
        self.pixelSizeFFT = None
        self.scanAreaX = None
        self.scanAreaY = None

        # self.__context = ApplicationContext(self, None)
        # self.__context.restoreLibrarySettings()

        self.logwidget = QTextEdit()
        self.logwidget.setReadOnly(True)

        # Initialize workflows
        self.preprocessWorkflow = PreprocessWorkflow()
        self.reconWorkflow = ReconWorkflow()

        # Settings for preprocessing
        self.preSettings = PreprocessingPanel(self)
        self.preSettings.sigDoPreprocess.connect(self.closeH5Files)
        self.preSettings.sigDoPreprocess.connect(self.doPreprocessWorkflow)

        # Settings for reconstruction
        self.reconSettings = ReconstructionPanel(self)
        self.reconSettings.sigDoRecon.connect(self.doReconWorkflow)
        
        self.reconSettings.sigCancel.connect(self.cancelReconWorkflow)

        self.reconShow = DisplayRecon(self)
        self.hdf5Tree = hdf5TreeView(parent=self, context=None)
        self.dataPanel = DataPanel(self.hdf5Tree)

        self.stages = {'Preprocessing'  : GUILayout(self.preSettings,
                                                    bottom=self.logwidget),
                       'Reconstruction' : GUILayout(self.reconShow,
                                                    left=self.reconSettings,
                                                    lefttop=PanelState.Disabled,
                                                    bottom=self.logwidget),
                       'Result Viewer' : GUILayout(self.dataPanel,
                                                   left=self.hdf5Tree,
                                                   lefttop=PanelState.Disabled)}
        super(CDIPlugin, self).__init__()

        self.logThread = threads.QThreadFuture(self.printLog, threadkey='logCDI', showBusy=False)
        self.logThread.start()

        self.displayImageThread = threads.QThreadFuture(self.displayImage, threadkey='displayImageCDI', showBusy=False)
        self.displayImageThread.start()

    def printLog(self):
        sock = CONTEXT.socket(zmq.SUB)
        while(1):
            try:
                sock.bind("tcp://*:" + str(self.zmqLogPort))
                sock.setsockopt_string(zmq.SUBSCRIBE, '')
                break
            except:
                self.zmqLogPort += 1

        while True:
            log = sock.recv().decode()
            if len(log):
                threads.invoke_in_main_thread(self.logwidget.append, log)

    def displayImage(self):
        sock = CONTEXT.socket(zmq.SUB)
        while(1):
            try:
                sock.bind("tcp://*:" + str(self.zmqImgPort))
                sock.setsockopt_string(zmq.SUBSCRIBE, '')
                break
            except:
                self.zmqImgPort += 1

        while True:
            name, array = recv_array(sock)

            if name == 'ObjectAmplitude':

                # Display Object Amplitude
                threads.invoke_in_main_thread(self.reconShow.plotObjAmp.addImage, array)

            if name == 'ObjectPhase':

                # Display Object Phase
                threads.invoke_in_main_thread(self.reconShow.plotObjPhase.addImage, array)

            if name == 'CalculatedIntensity':

                # Display Calculated Intensity
                threads.invoke_in_main_thread(self.reconShow.plotCalculatedIntensity.addImage, array)

            if name == 'ObservedIntensity':

                # Display Observed Intensity
                threads.invoke_in_main_thread(self.reconShow.plotObservedIntensity.addImage, array)



    def closeH5Files(self):
        self.hdf5Tree.close()

    @threads.method()
    def doPreprocessWorkflow(self):
        if self.preSettings.imageSourceType.value() == 'Single Tiff':
            imgDataPath = self.preSettings.imagePathFile.value()
        else:
            imgDataPath = self.preSettings.imageDataDir.value()

        params = { 'energy' : self.preSettings.energy.value(),
                   'pixelSize' : self.preSettings.pixelSize.value(),
                   'centerPixHeight' : self.preSettings.cch1.value(),
                   'centerPixWidth' : self.preSettings.cch2.value(),
                   'detectorDistance' : self.preSettings.detectorDistance.value(),
                   'saturationLevel' : self.preSettings.saturationLevel.value(),
                   'mask' : self.preSettings.mask.value(),
                   'noiseLevel' : self.preSettings.noiseLevel.value(),
                   'imgDataPath' : imgDataPath,
                   'delta' : self.preSettings.delta.value(),
                   'eta' : self.preSettings.eta.value(),
                   'chi' : self.preSettings.chi.value(),
                   'phi' : self.preSettings.phi.value(),
                   'nu' : self.preSettings.nu.value(),
                   'mu' : self.preSettings.mu.value(),
                   'etaStep' : self.preSettings.eta_step.value(),
                   'phiStep' : self.preSettings.phi_step.value(),
                   'comment' : self.preSettings.comment.value()}

        self.hdf5Tree.close()
        self.preprocessWorkflow.execute(None, params=params, zmqLogPort=self.zmqLogPort, threadkey='preprocess')

    def cancelReconWorkflow(self):
        if self.reconStatus:
            proc = self.reconWorkflow.reconPlugin.process
            proc.kill()

    @threads.method()
    def doReconWorkflow(self):
        args = []

        # CXI file
        args.append('data=' + self.reconSettings.imagePathParam.value())

        maskType = self.reconSettings.maskType.value()

        if maskType == 'zero':
            args.append('mask=zero')
        elif maskType == 'negative':
            args.append('mask=negative')
        elif maskType == 'file' and os.path.isfile(self.reconSettings.maskPath.value()):
            args.append('mask=' + self.reconSettings.maskPath.value())
        else:
            pass

        # Pixel Size
        args.append('pixel_size_detector=' + str(self.preSettings.pixelSize.value()))

        # Positivity
        args.append("positivity=" + str(int(self.reconSettings.positivity.value())))

        # Detector Distance
        args.append('detector_distance=' + str(self.preSettings.detectorDistance.value()))

        # Wavelength in meters
        energy = self.preSettings.energy.value() / 1000.
        wavelength = 12.39842 / energy * 1e-10 
        args.append('wavelength=' + str(wavelength))

        # Support type and size
        suppType = self.reconSettings.supportType.value()
        args.append('support=' + suppType)

        if suppType == 'square' or suppType == 'circle':
            args.append('support_size=' + str(self.reconSettings.supportSize.value()))

        # Support threshold
        args.append('support_threshold=' + str(self.reconSettings.supportThreshold.value()))
        args.append('support_threshold_method=' + str(self.reconSettings.supportThresholdMethod.value()))
        args.append('support_smooth_width_begin=' + str(self.reconSettings.supportSmoothWidthBegin.value()))
        args.append('support_smooth_width_end=' + str(self.reconSettings.supportSmoothWidthEnd.value()))
        args.append('support_smooth_width_relax_n=' + str(self.reconSettings.supportSmoothRelaxNum.value()))

        # Support shrink
        args.append('support_only_shrink=' + str(self.reconSettings.supportOnlyShrink.value()))



        # Number of run
        numOfRun = int(self.reconSettings.numRunParam.value())
        if numOfRun > 1:
            args.append('nb_run=' + str(numOfRun))
            # args.append('nb_run_keep=1') # Keep only the best run
            
        # Liveplot
        args.append('liveplot')

        # Verbose
        args.append('verbose=' + str(self.reconSettings.verbose.value()))

        # Algorithm
        args.append('algorithm=' + self.reconSettings.algorithm.value())

        # Crop output
        # args.append('crop_output=' + str(self.reconSettings.cropOutputParam.value()))

        # Do Reconstruction
        self.reconStatus = self.reconWorkflow.execute(None, args=args, zmqLogPort=self.zmqLogPort, zmqImgPort=self.zmqImgPort, threadkey='preprocess')

