import os
import glob
import sys
import subprocess
import zmq
import time

import h5py
import numpy as np
import xrayutilities as xu
from skimage.external import tifffile

from xicam.plugins import ProcessingPlugin, Input, Output
from pynx.version import __version__

def save_cdi_data_cxi(filename, iobs, wavelength=None, detector_distance=None, pixel_size_detector=None, mask=None,
                      sample_name=None, experiment_id=None, instrument=None, note=None, iobs_is_fft_shifted=False,
                      process_parameters=None):
    """
    Save the diffraction data (observed intensity, mask) to an HDF5 CXI file, NeXus-compatible.
    :param filename: the file name to save the data to
    :param iobs: the observed intensity
    :param wavelength: the wavelength of the experiment (in meters)
    :param detector_distance: the detector distance (in meters)
    :param pixel_size_detector: the pixel size of the detector (in meters)
    :param mask: the mask indicating valid (=0) and bad pixels (>0)
    :param sample_name: optional, the sample name
    :param experiment_id: the string identifying the experiment, e.g.: 'HC1234: Siemens star calibration tests'
    :param instrument: the string identifying the instrument, e.g.: 'ESRF id10'
    :param iobs_is_fft_shifted: if true, input iobs (and mask if any) have their origin in (0,0[,0]) and will be shifted
    back to centered-versions before being saved.
    :param process_parameters: a dictionary of parameters which will be saved as a NXcollection
    :return: Nothing. a CXI file is created
    """
    f = h5py.File(filename, "w")
    f.attrs['file_name'] = filename
    f.attrs['file_time'] = time.strftime("%Y-%m-%dT%H:%M:%S%z", time.localtime(time.time()))
    if instrument is not None:
        f.attrs['instrument'] = instrument
    f.attrs['creator'] = 'PyNX'
    # f.attrs['NeXus_version'] = '2018.5'  # Should only be used when the NeXus API has written the file
    f.attrs['HDF5_Version'] = h5py.version.hdf5_version
    f.attrs['h5py_version'] = h5py.version.version
    f.attrs['default'] = 'entry_1'
    f.create_dataset("cxi_version", data=140)

    entry_1 = f.create_group("entry_1")
    entry_1.create_dataset("program_name", data="PyNX %s" % (__version__))
    entry_1.create_dataset("start_time", data=time.strftime("%Y-%m-%dT%H:%M:%S%z", time.localtime(time.time())))
    entry_1.attrs['NX_class'] = 'NXentry'
    # entry_1.create_dataset('title', data='1-D scan of I00 v. mr')
    entry_1.attrs['default'] = 'data_1'

    if experiment_id is not None:
        entry_1.create_dataset("experiment_id", data=experiment_id)

    if note is not None:
        note_1 = entry_1.create_group("note_1")
        note_1.create_dataset("data", data=note)
        note_1.create_dataset("type", data="text/plain")

    if sample_name is not None:
        sample_1 = entry_1.create_group("sample_1")
        sample_1.create_dataset("name", data=sample_name)

    instrument_1 = entry_1.create_group("instrument_1")
    instrument_1.attrs['NX_class'] = 'NXinstrument'
    if instrument is not None:
        instrument_1.create_dataset("name", data=instrument)

    if wavelength is not None:
        nrj = 12.3984 / (wavelength * 1e10) * 1.60218e-16
        source_1 = instrument_1.create_group("source_1")
        source_1.attrs['NX_class'] = 'NXsource'
        source_1.attrs['note'] = 'Incident photon energy (instead of source energy), for CXI compatibility'
        source_1.create_dataset("energy", data=nrj)
        source_1["energy"].attrs['units'] = 'J'

        beam_1 = instrument_1.create_group("beam_1")
        beam_1.attrs['NX_class'] = 'NXbeam'
        beam_1.create_dataset("incident_energy", data=nrj)
        beam_1["incident_energy"].attrs['units'] = 'J'
        beam_1.create_dataset("incident_wavelength", data=wavelength)
        beam_1["incident_wavelength"].attrs['units'] = 'm'

    detector_1 = instrument_1.create_group("detector_1")
    detector_1.attrs['NX_class'] = 'NX_detector'

    if detector_distance is not None:
        detector_1.create_dataset("distance", data=detector_distance)
        detector_1["distance"].attrs['units'] = 'm'
    if pixel_size_detector is not None:
        detector_1.create_dataset("x_pixel_size", data=pixel_size_detector)
        detector_1["x_pixel_size"].attrs['units'] = 'm'
        detector_1.create_dataset("y_pixel_size", data=pixel_size_detector)
        detector_1["y_pixel_size"].attrs['units'] = 'm'
    if iobs_is_fft_shifted:
        detector_1.create_dataset("data", data=fftshift(iobs), chunks=True, shuffle=True,
                                  compression="gzip")
    else:
        detector_1.create_dataset("data", data=iobs, chunks=True, shuffle=True,
                                  compression="gzip")

    if mask is not None:
        if mask.sum() != 0:
            if iobs_is_fft_shifted:
                detector_1.create_dataset("mask", data=fftshift(mask), chunks=True, shuffle=True,
                                          compression="gzip")
            else:
                detector_1.create_dataset("mask", data=mask, chunks=True, shuffle=True, compression="gzip")
    if False:
        # Basis vector - this is the default CXI convention, so could be skipped
        # This corresponds to a 'top, left' origin convention
        basis_vectors = np.zeros((2, 3), dtype=np.float32)
        basis_vectors[0, 1] = -pixel_size_detector
        basis_vectors[1, 0] = -pixel_size_detector
        detector_1.create_dataset("basis_vectors", data=basis_vectors)

    data_1 = entry_1.create_group("data_1")
    data_1.attrs['NX_class'] = 'NXdata'
    data_1["data"] = h5py.SoftLink('/entry_1/instrument_1/detector_1/data')
    data_1.attrs['signal'] = 'data'
    if iobs.ndim == 3:
        data_1.attrs['interpretation'] = 'scaler'  # NeXus specs don't make sense
    else:
        data_1.attrs['interpretation'] = 'image'

    # Remember how import was done
    command = ""
    for arg in sys.argv:
        command += arg + " "
    process_1 = data_1.create_group("process_1")
    process_1.attrs['NX_class'] = 'NXprocess'
    process_1.create_dataset("program", data='PyNX')  # NeXus spec
    process_1.create_dataset("version", data="%s" % (__version__))  # NeXus spec
    process_1.create_dataset("command", data=command)  # CXI spec

    # Configuration & results of process: custom ESRF data policy
    # see https://gitlab.esrf.fr/sole/data_policy/blob/master/ESRF_NeXusImplementation.rst
    if process_parameters is not None:
        config = process_1.create_group("configuration")
        config.attrs['NX_class'] = 'NXcollection'
        for k, v in process_parameters.items():
            if v is not None:
                if type(v) is dict:
                    # This can happen if complex configuration is passed on
                    if len(v):
                        kd = config.create_group(k)
                        kd.attrs['NX_class'] = 'NXcollection'
                        for kk, vv in v.items():
                            kd.create_dataset(kk, data=vv)
                else:
                    config.create_dataset(k, data=v)

    f.close()


class PreprocessPlugin(ProcessingPlugin):
    params = Input(description='Preprocessing Parameters', type=dict)
    zmqLogPort = Input(description='ZeroMQ Log Port', type=int)

    def _print(self, msg, *args):
        """print msg to stdout and zmq port"""
        # print a string to stdout
        print(msg)
        # send to zmq
        if type(msg) == str:
            self.sock.send(msg.encode())

        for msg in args:
            if type(msg) == str:
                # print a string to stdout
                print(msg)
                # send to zmq
                self.sock.send(msg.encode())
            else:
                print(str(msg))

    def evaluate(self):
        #ZeroMQ Context
        CONTEXT = zmq.Context()
        self.sock = CONTEXT.socket(zmq.PUB)
        port = self.zmqLogPort.value
        self.sock.connect("tcp://localhost:"+str(port))

        self.imgDataPath = self.params.value['imgDataPath']

        # Load Images
        if os.path.isdir(self.imgDataPath):
            tifList = sorted(glob.glob(os.path.join(self.imgDataPath, '*.tif')))

            nz = len(tifList)
            firstImage = tifffile.imread(tifList[0])
            ny, nx = firstImage.shape[-2], firstImage.shape[-1]

            iobs = np.empty((nz, ny, nx), dtype=np.float32)
            idx = 0

            for _file in tifList:
                iobs[idx] = np.array(tifffile.imread(_file))
                idx += 1

        elif os.path.isfile(self.imgDataPath):
            iobs = np.array(tifffile.imread(self.imgDataPath))
            nz, ny, nx = iobs.shape

        else:
            return

        self._print("Image Loaded!")
        shape = iobs.shape

        # Convert to q-space

        # BL9C experiment geometry
        # mu, th, chi, phi, nu, tth
        # incident beam : x-axis
        # surface normal : z-axis
        qconv = xu.experiment.QConversion(['z-', 'y-', 'x+', 'z-'], ['z-','y-'], [1, 0, 0])
        hxrd = xu.HXRD((1, 0, 0), (0, 0, 1), en=self.params.value['energy'], qconv=qconv)
        hxrd.Ang2Q.init_area('z-', 'y+',
                             cch1=self.params.value['centerPixHeight'],
                             cch2=self.params.value['centerPixWidth'],
                             Nch1=shape[-2],
                             Nch2=shape[-1],
                             pwidth1=self.params.value['pixelSize'],
                             pwidth2=self.params.value['pixelSize'],
                             distance=self.params.value['detectorDistance'])

        qx = []
        qy = []
        qz = []

        for idx in range(nz):
            # mu, th(eta), chi, phi, nu, tth(delta)
            _qx, _qy, _qz=hxrd.Ang2Q.area(self.params.value['mu'],
                                          self.params.value['eta'] + self.params.value['etaStep'] * idx,
                                          self.params.value['chi'],
                                          self.params.value['phi'] + self.params.value['phiStep'] * idx,
                                          self.params.value['nu'],
                                          self.params.value['delta'])
            qx.append(_qx)
            qy.append(_qy)
            qz.append(_qz)

        # Grid data 
        qx = np.array(qx)
        qy = np.array(qy)
        qz = np.array(qz)

        qx_diff = qx.max() - qx.min()
        qy_diff = qy.max() - qy.min()
        qz_diff = qz.max() - qz.min()

        # reference to qy_step
        qy_step = qy_diff / ny

        grid_num_x = int(round(qx_diff / qy_step, 0))
        grid_num_y = ny
        grid_num_z = int(round(qz_diff / qy_step, 0))

        gridder = xu.FuzzyGridder3D(grid_num_x , grid_num_y, grid_num_z)
        # gridder = xu.FuzzyGridder3D(nz , ny, nx)
        gridder(qx, qy, qz, iobs)

        # # Save to an tif file
        # if os.path.isfile(self.imgDataPath):
        #     basePath = os.path.splitext(self.imgDataPath)[0]
        #     outFile = basePath + '_qconv.tif'
        # else:
        #     outFile = self.imgDataPath + '_qconv.tif'

        # tifffile.imsave(outFile, gridder.data)
        cxiFile = os.path.splitext(self.imgDataPath)[-2] + '.cxi'

        # wavelength in meter
        wavelength = 12.39842 / (float(self.params.value['energy']) / 1000.) * 1e-10

        data = gridder.data

        # Initialize mask
        if self.params.value['mask']:
            mask = np.zeros(data.shape)
            mask[data<=self.params.value['noiseLevel']] = 1
            mask[data<=self.params.value['saturationLevel']] = 1
        else:
            mask = None

 
        data[data<=self.params.value['noiseLevel']] = 0

        # Save data to cxi
        save_cdi_data_cxi(cxiFile, 
                          data, 
                          wavelength, 
                          self.params.value['detectorDistance'], 
                          self.params.value['pixelSize'], 
                          mask, 
                          note=self.params.value['comment'])
        self._print("Data file is  created", cxiFile)

