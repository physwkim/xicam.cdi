__author__ = "Sang-Woo Kim, Pohang Accelerator Laboratory"
__contact__ = "physwkim@postech.ac.kr"
__license__ = "MIT"
__copyright__ = "Pohang Accelerator Laboratory, Pohang, South Korea"

import sys
from silx.gui import qt
# from xicam.cdi.PlotWidgetCustom import PlotWidget
from silx.gui.plot import PlotWidget as _PlotWidget
from silx.gui.plot.actions.control import ZoomBackAction, ColormapAction

class PlotWidget(_PlotWidget):
    """Customized PlotWidget"""
    def __init__(self, *args, **kwargs):
        super(PlotWidget, self).__init__(*args, **kwargs)

        # Retrieve PlotWidget's plot area widget
        plotArea = self.getWidgetHandle()

        # Create QAction for the context menu once for all
        self._zoomBackAction = ZoomBackAction(plot=self, parent=self)
        self._colormapAction = ColormapAction(plot=self, parent=self)

        # Axis label
        self.setGraphXLabel('x (pix.)')
        self.setGraphYLabel('y (pix.)')

        # Set plot area custom context menu
        plotArea.setContextMenuPolicy(qt.Qt.CustomContextMenu)
        plotArea.customContextMenuRequested.connect(self._contextMenu)

        # Set default colormap to 'viridis'
        self.getDefaultColormap().setName('viridis')

    def setGraphTitle(self, title="", loc="center"):
        self._graphTitle = str(title)
        self._backend.ax.set_title(title, loc=loc)
        self._setDirtyPlot()

    def _contextMenu(self, pos):
        """Handle plot area customContextMenuRequested signal.

        :param QPoint pos: Mouse position relative to plot area
        """
        # Create the context menu
        menu = qt.QMenu(self)
        menu.addAction(self._zoomBackAction)
        menu.addSeparator()
        menu.addAction(self._colormapAction)
        menu.addSeparator()

        # Displaying the context menu at the mouse position requires
        # a global position.
        # The position received as argument is relative to PlotWidget's
        # plot area, and thus needs to be converted.
        plotArea = self.getWidgetHandle()
        globalPosition = plotArea.mapToGlobal(pos)
        menu.exec_(globalPosition)

    def setMouseText(self, text=""):
        try:
            if len(text):
                qt.QToolTip.showText(self.cursor().pos(),
                                     text, self, qt.QRect())
            else:
                qt.QToolTip.hideText()
        except:
            print("Error trying to show mouse text <%s>" % text)
    
    def graphCallback(self, ddict=None):
        """This callback is going to receive all the events from the plot.

        Those events will consist on a dictionary and among the dictionary
        keys the key 'event' is mandatory to describe the type of event.
        This default implementation only handles setting the active curve.
        """

        if ddict is None:
            ddict = {}
        if ddict['event'] in ["legendClicked", "curveClicked"]:
            if ddict['button'] == "left":
                self.setActiveCurve(ddict['label'])
                qt.QToolTip.showText(self.cursor().pos(), ddict['label'])

        if ddict['event'] in ['mouseMoved','MouseAt', 'mouseClicked']:
            # self._handleMouseMovedEvent(ddict)
            # print("ddict=", ddict)
            xPixel = ddict['xpixel']
            yPixel = ddict['ypixel']
            try:
                if self._isPositionInPlotArea(xPixel, yPixel) == (xPixel, yPixel):
                    data = self.getImage().getData()

                    dataPos = [ddict['x'], ddict['y']]
                    origin = self.getImage().getOrigin()
                    scale = self.getImage().getScale()

                    column = int((dataPos[0] - origin[0]) / float(scale[0]))
                    row = int((dataPos[1] - origin[1]) / float(scale[1]))

                    if scale[0] is not None:
                        x = origin[0] + column * scale[0]
                        y = origin[1] + row * scale[1]
                    else:
                        x = column
                        y = row

                if column < data.shape[1] and row < data.shape[0]:
                    self.setMouseText("%g, %g, %g" % (x, y, data[row, column]))

                # Update energy-spectrum with mouse click
                # if ddict['event'] is 'mouseClicked':
                #     pos = {'x':column, 'y':row, 'xData':x, 'yData':y}
                #     self.sigSpectrumSelected.emit(pos)
            except:
                # print("Error on displaying data tooltip on PlotWidgetCustom")
                pass

        self.sigPlotSignal.emit(ddict)


class DisplayRecon(qt.QMainWindow):
    """Shows the amplitude and phase of probe and object during
       cdi reconstruction
    """

    def __init__(self, parent=None, *args, **kwargs):
        super(DisplayRecon, self).__init__(*args, **kwargs)
        self.parent = parent
        self.create_widgets()
        self.layout_widgets()
        
    def create_widgets(self):
        """Create widgets"""
        self.main_panel = qt.QWidget(self)
        self.setCentralWidget(self.main_panel)
        self.plotObjAmp = PlotWidget(self)
        self.plotObjAmp.setGraphTitle('Object Amplitude', loc='center')
        self.plotObjPhase = PlotWidget(self)
        self.plotObjPhase.setGraphTitle('Object Phase', loc='center')

        self.plotCalculatedIntensity = PlotWidget(self)
        self.plotCalculatedIntensity.setGraphTitle('Calculated Intensity', loc='center')
        self.plotObservedIntensity = PlotWidget(self)
        self.plotObservedIntensity.setGraphTitle('Measured Intensity', loc='center')


    def layout_widgets(self):
        gridLayout = qt.QGridLayout()
        gridLayout.setSpacing(0)
        gridLayout.setContentsMargins(0, 0, 0, 0)
        gridLayout.addWidget(self.plotObjAmp, 0, 0)
        gridLayout.addWidget(self.plotObjPhase, 0, 1)
        gridLayout.addWidget(self.plotCalculatedIntensity, 1, 0)
        gridLayout.addWidget(self.plotObservedIntensity, 1, 1)
        # gridLayout.setRowStretch(1, 1)
        # gridLayout.setColumnStretch(1, 1)

        self.main_panel.setLayout(gridLayout)

if __name__ == '__main__':
    app = qt.QApplication(sys.argv)
    main = DisplayRecon()
    main.show()
    sys.exit(app.exec_())
