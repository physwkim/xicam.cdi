from silx.gui.plot.ProfileMainWindow import ProfileMainWindow

class ProfileMainWindowCustom(ProfileMainWindow):
    def __init__(self, parent=None):
        self._parent=parent
        super(ProfileMainWindowCustom, self).__init__(parent)

    def setProfileType(self, profileType):
        """Set which profile plot widget (1D or 2D) is to be used

        :param str profileType: Type of profile data,
            "1D" for a curve or "2D" for an image
        """
        # import here to avoid circular import
        from xicam.cdi.PlotWindowCustom import Plot1DCustom
        from silx.gui.plot.PlotWindow import Plot2D      # noqa
        self._profileType = profileType
        if self._profileType == "1D":
            if self._plot2D is not None:
                self._plot2D.setParent(None)   # necessary to avoid widget destruction
            if self._plot1D is None:
                self._plot1D = Plot1DCustom(parent=self._parent)
                self._plot1D.setGraphYLabel('Profile')
                self._plot1D.setGraphXLabel('')
            self.setCentralWidget(self._plot1D)
        elif self._profileType == "2D":
            if self._plot1D is not None:
                self._plot1D.setParent(None)   # necessary to avoid widget destruction
            if self._plot2D is None:
                self._plot2D = Plot2D()
            self.setCentralWidget(self._plot2D)
        else:
            raise ValueError("Profile type must be '1D' or '2D'")

        self.sigProfileDimensionsChanged.emit(profileType)
