from silx.gui import qt
from silx.gui import icons
from silx.gui.plot.PlotToolButtons import PlotToolButton


class ScanAreaToolButton(PlotToolButton):
    """Tool button to draw scan area box"""

    STATE = None
    """Lazy loaded states used to feed AspectToolButton"""

    def __init__(self, parent=None, plot=None):
        if self.STATE is None:
            self.STATE = {}
            # dont keep ratio
            self.STATE[False, "icon"] = icons.getQIcon('image')
            self.STATE[False, "state"] = "Do not draw a box on the plot showing the scan area"
            self.STATE[False, "action"] = "Do not draw a box on the plot showing the scan area"
            # keep ratio
            self.STATE[True, "icon"] = icons.getQIcon('image-select-box')
            self.STATE[True, "state"] = "Do draw a box on the plot showing the scan area" 
            self.STATE[True, "action"] = "Do draw a box on the plot showing the scan area" 

        super(ScanAreaToolButton, self).__init__(parent=parent, plot=plot)

        drawAction = self._createAction(True)
        drawAction.triggered.connect(self.keepDataAspectRatio)
        drawAction.setIconVisibleInMenu(True)

        dontDrawAction = self._createAction(False)
        dontDrawAction.triggered.connect(self.dontKeepDataAspectRatio)
        dontDrawAction.setIconVisibleInMenu(True)

        menu = qt.QMenu(self)
        menu.addAction(drawAction)
        menu.addAction(dontDrawAction)
        self.setMenu(menu)
        self.setPopupMode(qt.QToolButton.InstantPopup)

    def _createAction(self, Draw):
        icon = self.STATE[Draw, "icon"]
        text = self.STATE[Draw, "action"]
        return qt.QAction(icon, text, self)

    def _drawScanArea(self, plot):
        plot.__drawScanArea = True

    def _dontDrawScanArea(self, plot):
        plot.__drawScanArea = False

    def keepDataAspectRatio(self):
        """Configure the plot to keep the aspect ratio"""
        plot = self.plot()
        if plot is not None:
            # This will trigger _keepDataAspectRatioChanged
            plot.setKeepDataAspectRatio(True)

    def dontKeepDataAspectRatio(self):
        """Configure the plot to not keep the aspect ratio"""
        plot = self.plot()
        if plot is not None:
            # This will trigger _keepDataAspectRatioChanged
            plot.setKeepDataAspectRatio(False)

    def _keepDataAspectRatioChanged(self, aspectRatio):
        """Handle Plot set keep aspect ratio signal"""
        icon, toolTip = self.STATE[aspectRatio, "icon"], self.STATE[aspectRatio, "state"]
        self.setIcon(icon)
        self.setToolTip(toolTip)
