import sys
import os
from collections import OrderedDict

from silx.gui import qt

import pyqtgraph.parametertree.parameterTypes as pTypes

from pyqtgraph.parametertree import Parameter, \
                                    ParameterTree, \
                                    ParameterItem

from xicam.cdi.pathParameter import FilePathParameter, \
                                             DirPathParameter, \
                                             LabelParameter, \
                                             SimpleParameter

class StxmFilePanel(ParameterTree):
    sigLoadFile = qt.pyqtSignal()

    def __init__(self, parent, *args, **kwargs):
        super(StxmFilePanel, self).__init__(*args, **kwargs)
        self.parent = parent

        # Header file path
        self.filePath = FilePathParameter(name='CXI File Path',
                                          type='str',
                                          value='',
                                          dir=True,
                                          fileType='cxi (*.cxi)')

        self.filePath.sigValueChanged.connect(self.sigLoadFile)

        self.parameter = pTypes.GroupParameter(name='CXI File Path',
                                               children=[self.filePath])
        self.setParameters(self.parameter, showTop=False)
