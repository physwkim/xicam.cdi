import sys
import os
import signal
import trace

os.environ["QT_API"] = "pyqt5"
from silx.gui import qt

qt.QCoreApplication.setOrganizationName("Camera")
qt.QCoreApplication.setApplicationName("Xi-cam")


def main():
    app = qt.QApplication([])
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    from xicam.cdi.main import MainWindow

    main = MainWindow()
    main.show()
    app.exec_()


if __name__ == "__main__":
    main()
