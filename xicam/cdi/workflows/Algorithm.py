# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2017-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr


import warnings
import types
import timeit
import time
import sys
import gc
from random import randint
import numpy as np
import zmq
import h5py
from scipy.fftpack import fftn, ifftn, fftshift
from scipy.ndimage.measurements import center_of_mass

from pynx.processing_unit.cu_processing_unit import CUProcessingUnit
from pynx.processing_unit.kernel_source import get_kernel_source as getks
from pynx.processing_unit import default_processing_unit as main_default_processing_unit
import pycuda.driver as cu_drv
import pycuda.gpuarray as cua
import pycuda.tools as cu_tools
from pycuda.elementwise import ElementwiseKernel as CU_ElK
from pycuda.reduction import ReductionKernel as CU_RedK
from pycuda.compiler import SourceModule
import skcuda.fft as cu_fft

from pynx.version import __version__
from pynx.operator import has_attr_not_none
from pynx.cdi.cdi import CDI as _CDI
from pynx.cdi.cpu_operator import ShowCDI as _ShowCDI
from pynx.cdi.cu_operator import FT, IFT, FourierApplyAmplitude, ERProj, CFProj
from pynx.cdi.cu_operator import HIOProj, CopyToPrevious, RAARProj, ApplyAmplitude
from pynx.cdi.cu_operator import ER as _ER, HIO as _HIO, RAAR as _RAAR, GPS as _GPS, CF as _CF, ML as _ML

def send_array(socket, name, A, flags=0, copy=True, track=False):
    """send a numpy array with metadata"""
    md = dict(
            name  = name,
            dtype = str(A.dtype),
            shape = A.shape,
            )
    socket.send_json(md, flags|zmq.SNDMORE)
    return socket.send(A, flags, copy=copy, track=track)


def recv_array(socket, flags=0, copy=True, track=False):
    """recv a numpy array"""
    md = socket.recv_json(flags=flags)
    msg = socket.recv(flags=flags, copy=copy, track=track)
    buf = memoryview(msg)
    name = md['name']
    A = np.frombuffer(buf, dtype=md['dtype'])
    return name, A.reshape(md['shape'])

"""
CDI
"""
class CDI(_CDI):
    """
    Reconstruction class for two or three-dimensional coherent diffraction imaging data.
    """

    def __init__(self, sock, *args, **kwargs):
        super(CDI, self).__init__(*args, **kwargs)

        #ZeroMQ Context
        self.sock = sock

    def _print(self, msg):
        """print msg to stdout and zmq port"""
        # print a string to stdout
        print(msg)

        # send to zmq
        self.sock.send(msg.encode())

    def update_history(self, mode='llk', verbose=False, **kwargs):
        """
        Update the history record.
        :param mode: either 'llk' (will record new log-likelihood, nb_photons_calc, average value..),
        or 'algorithm' (will only update the algorithm) or 'support'.
        :param verbose: if True, print some info about current process
        :param kwargs: other parameters to be recorded, e.g. support_threshold=xx, dt=
        :return: nothing
        """
        if mode == 'llk':
            llk = self.get_llk()
            algo = ''
            dt = 0
            if 'algorithm' in kwargs:
                algo = kwargs['algorithm']
            if 'dt' in kwargs:
                dt = kwargs['dt']
            if verbose:
                self._print("%4s #%3d LLK= %8.2f[%8.3f](p) %8.3f[%8.3f](g) %8.3f[%8.3f](e), nb photons=%e, "
                          "support:nb=%6d (%6.3f%%) average=%10.2f max=%10.2f, dt/cycle=%6.4fs" % (
                          algo, self.cycle, llk[0], llk[3], llk[1], llk[4], llk[2], llk[5], self.nb_photons_calc,
                          self.nb_point_support, self.nb_point_support / self._obj.size * 100,
                          np.sqrt(self.nb_photons_calc / self.nb_point_support), self._obj_max, dt))
            self.history.insert(self.cycle, llk_poisson=llk[0], llk_poisson_free=llk[3],
                                llk_gaussian=llk[1], llk_gaussian_free=llk[4], llk_euclidian=llk[2],
                                llk_euclidian_free=llk[2], nb_photons_calc=self.nb_photons_calc,
                                obj_average=np.sqrt(self.nb_photons_calc / self.nb_point_support), **kwargs)
        elif mode == 'support':
            self.history.insert(self.cycle, support_size=self.nb_point_support, obj_max=self._obj_max,
                                obj_average=np.sqrt(self.nb_photons_calc / self.nb_point_support))
        elif 'algo' in mode:
            if 'algorithm' in kwargs:
                self.history.insert(self.cycle, algorithm=kwargs['algorithm'])

    def save_obj_cxi(self, filename, sample_name=None, experiment_id=None, instrument=None, note=None, crop=0,
                     save_psf=False, process_notes=None, process_parameters=None, append=False, **kwargs):
        """
        Save the result of the optimisation (object, support) to an HDF5 CXI file.
        :param filename: the file name to save the data to
        :param sample_name: optional, the sample name
        :param experiment_id: the string identifying the experiment, e.g.: 'HC1234: Siemens star calibration tests'
        :param instrument: the string identifying the instrument, e.g.: 'ESRF id10'
        :param note: a string with a text note giving some additional information about the data, a publication...
        :param crop: integer, if >0, the object will be cropped arount the support, plus a margin of 'crop' pixels.
        :param save_psf: if True, also save the psf (if present)
        :param process_notes: a dictionary which will be saved in '/entry_N/process_1'. A dictionary entry
        can also be a 'note' as keyword and a dictionary as value - all key/values will then be saved
        as separate notes. Example: process={'program': 'PyNX', 'note':{'llk':1.056, 'nb_photons': 1e8}}
        :param process_parameters: a dictionary of parameters which will be saved as a NXcollection
        :param append: if True and the file already exists, the result will be saved in a new entry.
        :return: Nothing. a CXI file is created
        """

        if 'process' in kwargs:
            warnings.warn("CDI.save_obj_cxi(): parameter 'process' is deprecated,"
                          "use process_notes and process_parameters instead.")
            if process_notes is None:
                process_notes = kwargs['process']
            else:
                warnings.warn("CDI.save_obj_cxi(): parameter 'process' ignored as 'process_notes' is also given")

        if append:
            f = h5py.File(filename, "a")
            if "cxi_version" not in f:
                f.create_dataset("cxi_version", data=150)
            i = 1
            while True:
                if 'entry_%d' % i not in f:
                    break
                i += 1
            entry = f.create_group("/entry_%d" % i)
            entry_path = "/entry_%d" % i
            f.attrs['default'] = "/entry_%d" % i
            if "/entry_last" in entry:
                del entry["/entry_last"]
            entry["/entry_last"] = h5py.SoftLink("/entry_%d" % i)
        else:
            f = h5py.File(filename, "w")
            f.create_dataset("cxi_version", data=150)
            entry = f.create_group("/entry_1")
            entry_path = "/entry_1"
            f.attrs['default'] = 'entry_1'
            entry["/entry_last"] = h5py.SoftLink("/entry_1")

        f.attrs['creator'] = 'PyNX'
        # f.attrs['NeXus_version'] = '2018.5'  # Should only be used when the NeXus API has written the file
        f.attrs['HDF5_Version'] = h5py.version.hdf5_version
        f.attrs['h5py_version'] = h5py.version.version

        entry.attrs['NX_class'] = 'NXentry'
        # entry.create_dataset('title', data='1-D scan of I00 v. mr')
        entry.attrs['default'] = 'data_1'
        entry.create_dataset("program_name", data="PyNX %s" % (__version__))
        entry.create_dataset("start_time", data=time.strftime("%Y-%m-%dT%H:%M:%S%z", time.localtime(time.time())))
        if experiment_id is not None:
            entry.create_dataset("experiment_id", data=experiment_id)

        if note is not None:
            note_1 = entry.create_group("note_1")
            note_1.create_dataset("data", data=note)
            note_1.create_dataset("type", data="text/plain")

        if sample_name is not None:
            sample_1 = entry.create_group("sample_1")
            sample_1.attrs['NX_class'] = 'NXsample'
            sample_1.create_dataset("name", data=sample_name)

        obj = self.get_obj(shift=True)
        sup = self.get_support(shift=True)
        if crop > 0:
            # crop around support
            if self.iobs.ndim == 3:
                l0 = np.nonzero(sup.sum(axis=(1, 2)))[0].take([0, -1]) + np.array([-crop, crop])
                if l0[0] < 0: l0[0] = 0
                if l0[1] >= sup.shape[0]: l0[1] = -1

                l1 = np.nonzero(sup.sum(axis=(0, 2)))[0].take([0, -1]) + np.array([-crop, crop])
                if l1[0] < 0: l1[0] = 0
                if l1[1] >= sup.shape[1]: l1[1] = -1

                l2 = np.nonzero(sup.sum(axis=(0, 1)))[0].take([0, -1]) + np.array([-crop, crop])
                if l2[0] < 0: l2[0] = 0
                if l2[1] >= sup.shape[2]: l2[1] = -1
                obj = obj[l0[0]:l0[1], l1[0]:l1[1], l2[0]:l2[1]]
                sup = sup[l0[0]:l0[1], l1[0]:l1[1], l2[0]:l2[1]]
            else:
                l0 = np.nonzero(sup.sum(axis=1))[0].take([0, -1]) + np.array([-crop, crop])
                if l0[0] < 0: l0[0] = 0
                if l0[1] >= sup.shape[0]: l0[1] = -1

                l1 = np.nonzero(sup.sum(axis=0))[0].take([0, -1]) + np.array([-crop, crop])
                if l1[0] < 0: l1[0] = 0
                if l1[1] >= sup.shape[1]: l1[1] = -1

                obj = obj[l0[0]:l0[1], l1[0]:l1[1]]
                sup = sup[l0[0]:l0[1], l1[0]:l1[1]]

        image_1 = entry.create_group("image_1")
        image_1.create_dataset("data", data=obj, chunks=True, shuffle=True, compression="gzip")
        image_1.attrs['NX_class'] = 'NXdata'  # Is image_1 a well-formed NXdata or not ?
        image_1.attrs['signal'] = 'data'
        if obj.ndim == 3:
            image_1.attrs['interpretation'] = 'scaler'  # NeXus specs don't make sense
        else:
            image_1.attrs['interpretation'] = 'image'
        image_1.create_dataset("mask", data=sup, chunks=True, shuffle=True, compression="gzip")
        image_1["support"] = h5py.SoftLink('%s/image_1/mask' % entry_path)
        image_1.create_dataset("data_type", data="electron density")  # CXI, not NeXus
        image_1.create_dataset("data_space", data="real")  # CXI, not NeXus ?
        if self.pixel_size_object is not None:
            s = self.pixel_size_object * np.array(obj.shape)
            image_1.create_dataset("image_size", data=s)
            image_1.create_dataset("pixel_size_object", data=self.pixel_size_object)
            if False:  # TODO: x, y (z) axes description (scale), when accessible
                # See proposeition: https://www.nexusformat.org/2014_axes_and_uncertainties.html
                #
                # The following is only true if pixel size is the same along X and Y. Not always the case !
                ny, nx = obj.shape[-2:]
                x = (np.arange(nx) - nx // 2) * self.pixel_size_object
                y = (np.arange(ny) - ny // 2) * self.pixel_size_object
                image_1.create_dataset("x", data=x)
                image_1.create_dataset("y", data=y)
                image_1.attrs['axes'] = ". y x"  # How does this work ? "x y" or ['x', 'y'] ????
                # image_1.attrs['x_indices'] = [-1, ]
                # image_1.attrs['y_indices'] = [-2, ]

        instrument_1 = image_1.create_group("instrument_1")
        instrument_1.attrs['NX_class'] = 'NXinstrument'
        if instrument is not None:
            instrument_1.create_dataset("name", data=instrument)

        if self.wavelength is not None:
            nrj = 12.3984 / (self.wavelength * 1e10) * 1.60218e-16
            source_1 = instrument_1.create_group("source_1")
            source_1.attrs['NX_class'] = 'NXsource'
            source_1.attrs['note'] = 'Incident photon energy (instead of source energy), for CXI compatibility'
            source_1.create_dataset("energy", data=nrj)
            source_1["energy"].attrs['units'] = 'J'

            beam_1 = instrument_1.create_group("beam_1")
            beam_1.attrs['NX_class'] = 'NXbeam'
            beam_1.create_dataset("incident_energy", data=nrj)
            beam_1["incident_energy"].attrs['units'] = 'J'
            beam_1.create_dataset("incident_wavelength", data=self.wavelength)
            beam_1["incident_wavelength"].attrs['units'] = 'm'

        detector_1 = instrument_1.create_group("detector_1")
        detector_1.attrs['NX_class'] = 'NXdetector'
        if self.detector_distance is not None:
            detector_1.create_dataset("distance", data=self.detector_distance)
            detector_1["distance"].attrs['units'] = 'm'

        if self.pixel_size_detector is not None:
            detector_1.create_dataset("x_pixel_size", data=self.pixel_size_detector)
            detector_1["x_pixel_size"].attrs['units'] = 'm'
            detector_1.create_dataset("y_pixel_size", data=self.pixel_size_detector)
            detector_1["y_pixel_size"].attrs['units'] = 'm'

        if self._k_psf is not None and save_psf:
            # Likely this is due to partial coherence and not the detector, but it is still analysed as a PSF
            # TODO: only save center of psf
            detector_1.create_dataset("point_spread_function", data=self._k_psf, chunks=True, shuffle=True,
                                      compression="gzip")

        # Add shortcut to the main data
        data_1 = entry.create_group("data_1")
        data_1["data"] = h5py.SoftLink('%s/image_1/data' % entry_path)
        data_1.attrs['NX_class'] = 'NXdata'
        data_1.attrs['signal'] = 'data'
        if obj.ndim == 3:
            data_1.attrs['interpretation'] = 'scaler'  # NeXus specs don't make sense'
        else:
            data_1.attrs['interpretation'] = 'image'
        # TODO: x, y (z) axes description (scale), when accessible

        command = ""
        for arg in sys.argv:
            command += arg + " "
        process_1 = image_1.create_group("process_1")
        process_1.attrs['NX_class'] = 'NXprocess'
        process_1.create_dataset("program", data='PyNX')  # NeXus spec
        process_1.create_dataset("version", data="%s" % __version__)  # NeXus spec
        process_1.create_dataset("command", data=command)  # CXI spec

        if process_notes is not None:  # Notes
            for k, v in process_notes.items():
                if isinstance(v, str):
                    if k not in process_1:
                        process_1.create_dataset(k, data=v)
                elif isinstance(v, dict) and k == 'note':
                    # Save this as notes:
                    for kk, vv in v.items():
                        i = 1
                        while True:
                            note_s = 'note_%d' % i
                            if note_s not in process_1:
                                break
                            i += 1
                        note = process_1.create_group(note_s)
                        note.attrs['NX_class'] = 'NXnote'
                        note.create_dataset("description", data=kk)
                        # TODO: also save values as floating-point if appropriate
                        note.create_dataset("data", data=str(vv))
                        note.create_dataset("type", data="text/plain")

        # Configuration & results of process: custom ESRF data policy
        # see https://gitlab.esrf.fr/sole/data_policy/blob/master/ESRF_NeXusImplementation.rst
        config = process_1.create_group("configuration")
        config.attrs['NX_class'] = 'NXcollection'
        if process_parameters is not None:
            for k, v in process_parameters.items():
                if k == 'free_pixel_mask':
                    k = 'free_pixel_mask_input'
                if v is not None:
                    if type(v) is dict:
                        # This can happen if complex configuration is passed on
                        if len(v):
                            kd = config.create_group(k)
                            kd.attrs['NX_class'] = 'NXcollection'
                            for kk, vv in v.items():
                                kd.create_dataset(kk, data=vv)
                    else:
                        config.create_dataset(k, data=v)
        if self.iobs is not None and 'iobs_shape' not in config:
            config.create_dataset('iobs_shape', data=self.iobs.shape)

        # Save the free pixel mask so that it can be re-used
        mask_free_pixel = self.get_free_pixel_mask()
        if mask_free_pixel.sum():
            config.create_dataset('free_pixel_mask', data=fftshift(mask_free_pixel.astype(np.bool)), chunks=True,
                                  shuffle=True, compression="gzip")
            config['free_pixel_mask'].attrs['note'] = "Mask of pixels used for free log-likelihood calculation"
            if mask_free_pixel.ndim == 3:
                config['free_pixel_mask'].attrs['interpretation'] = 'scaler'
            else:
                config['free_pixel_mask'].attrs['interpretation'] = 'image'

        results = process_1.create_group("results")
        results.attrs['NX_class'] = 'NXcollection'
        llk = self.get_llk(normalized=True)
        results.create_dataset('llk_poisson', data=llk[0])
        results.create_dataset('llk_gaussian', data=llk[1])
        results.create_dataset('llk_euclidian', data=llk[2])
        results.create_dataset('free_llk_poisson', data=llk[3])
        results.create_dataset('free_llk_gaussian', data=llk[4])
        results.create_dataset('free_llk_euclidian', data=llk[5])
        results.create_dataset('nb_point_support', data=self.nb_point_support)
        results.create_dataset('cycle_history', data=self.history.as_numpy_record_array())
        for k in self.history.keys():
            results.create_dataset('cycle_history_%s' % k, data=self.history[k].as_numpy_record_array())

        f.close()



class ShowCDI(_ShowCDI):
    """
        Plot the current estimate of the object amplitude and phase, as well as a comparison of the calculated
        and observed intensities. For 3D data, a 2D cut is shown.
        
        NB: the object must be copied from PU space before
        NB: this is a CPU version, which will not display the calculated intensity
    """

    def __init__(self, zmq_img_sock, fig_num=None, i=None):
        """

        :param fig_num: the matplotlib figure number. if None, a new figure will be created each time.
        :param i: if the object is 3D, display the ith plane (default: the center one)
        """
        super(ShowCDI, self).__init__()
        self.sock = zmq_img_sock
        self.fig_num = fig_num
        self.i = i

    def op(self, cdi):
        if cdi._obj.ndim == 3:
            if self.i is not None:
                i = self.i - len(cdi.get_obj()) // 2
            else:
                v = np.sum(cdi.get_support(), axis=(1, 2))
                i = int(round(center_of_mass(fftshift(v))[0])) - len(cdi.get_obj()) // 2
            obj = cdi.get_obj()[i]
            iobs = cdi.get_iobs()[0].copy()
            icalc = self.get_icalc(cdi, 0)
            if cdi._support is not None:
                support = cdi.get_support()[i]
        else:
            obj = cdi.get_obj()
            iobs = cdi.get_iobs().copy()
            icalc = self.get_icalc(cdi)
            support = cdi.get_support()

        tmp = np.logical_and(iobs > -1e30, iobs < 0)
        if tmp.sum() > 0:
            # change back free pixels to their real intensity
            iobs[tmp] = -iobs[tmp] - 1

        objAmp = fftshift(abs(obj))
        objPhase = fftshift(np.angle(obj))
        calculatedInt = np.log10(fftshift(icalc))
        observedInt = np.log10(fftshift(iobs))

        # Object Amplitude
        send_array(self.sock, 'ObjectAmplitude', objAmp)

        # Object Phase
        send_array(self.sock, 'ObjectPhase', objPhase)

        # Calculated Intensity
        send_array(self.sock, 'CalculatedIntensity', calculatedInt)

        # Observed Intensity
        send_array(self.sock, 'ObservedIntensity', observedInt)

        return cdi
        
    @staticmethod
    def get_icalc(cdi, i=None):
        if cdi.in_object_space():
            cdi = FT(scale=True) * cdi
            icalc = abs(cdi.get_obj()) ** 2
            cdi = IFT(scale=True) * cdi
        else:
            icalc = abs(cdi.get_obj()) ** 2
        if icalc.ndim == 3 and i is not None:
            return icalc[i]
        return icalc


class ER(_ER):
    """
    Error reduction cycle
    """

    def __init__(self, sock, *args, **kwargs):
        super(ER, self).__init__(*args, **kwargs)
        self.sock = sock

    def __pow__(self, n):
        """

        :param n: a strictly positive integer
        :return: a new AP operator with the number of cycles multiplied by n
        """
        assert isinstance(n, int) or isinstance(n, np.integer)
        return ER(self.sock, positivity=self.positivity, calc_llk=self.calc_llk, nb_cycle=self.nb_cycle * n,
                  show_cdi=self.show_cdi, fig_num=self.fig_num, zero_mask=self.zero_mask)

    def op(self, cdi: CDI):
        t0 = timeit.default_timer()
        ic_dt = 0
        for ic in range(self.nb_cycle):
            calc_llk = False
            if self.calc_llk:
                if cdi.cycle % self.calc_llk == 0:
                    calc_llk = True
            cdi = ERProj(positivity=self.positivity) * FourierApplyAmplitude(calc_llk=calc_llk,
                                                                             zero_mask=self.zero_mask) * cdi

            if calc_llk:
                # Average time/cycle over the last N cycles
                dt = (timeit.default_timer() - t0) / (ic - ic_dt + 1)
                ic_dt = ic + 1
                t0 = timeit.default_timer()
                llk = cdi.get_llk()
                cdi.update_history(mode='llk', dt=dt, algorithm='ER', verbose=True)
            else:
                cdi.history.insert(cdi.cycle, algorithm='ER')
            if self.show_cdi:
                if cdi.cycle % self.show_cdi == 0:
                    cdi = ShowCDI(self.sock) * cdi
            cdi.cycle += 1
        return cdi

class CF(_CF):
    """
    Charge flipping cycle
    """

    def __init__(self, sock, *args, **kwargs):
        super(CF, self).__init__(*args, **kwargs)
        self.sock = sock

    def __pow__(self, n):
        """

        :param n: a strictly positive integer
        :return: a new CF operator with the number of cycles multiplied by n
        """
        assert isinstance(n, int) or isinstance(n, np.integer)
        return CF(self.sock, positivity=self.positivity, calc_llk=self.calc_llk, nb_cycle=self.nb_cycle * n,
                  show_cdi=self.show_cdi, fig_num=self.fig_num, zero_mask=self.zero_mask)

    def op(self, cdi: CDI):
        t0 = timeit.default_timer()
        ic_dt = 0
        for ic in range(self.nb_cycle):
            calc_llk = False
            if self.calc_llk:
                if cdi.cycle % self.calc_llk == 0:
                    calc_llk = True
            cdi = CFProj(positivity=self.positivity) * FourierApplyAmplitude(calc_llk=calc_llk,
                                                                             zero_mask=self.zero_mask) * cdi

            if calc_llk:
                # Average time/cycle over the last N cycles
                dt = (timeit.default_timer() - t0) / (ic - ic_dt + 1)
                ic_dt = ic + 1
                t0 = timeit.default_timer()
                llk = cdi.get_llk()
                cdi.update_history(mode='llk', dt=dt, algorithm='CF', verbose=True)
            else:
                cdi.history.insert(cdi.cycle, algorithm='CF')
            if self.show_cdi:
                if cdi.cycle % self.show_cdi == 0:
                    cdi = ShowCDI(self.sock) * cdi
            cdi.cycle += 1
        return cdi


class HIO(_HIO):
    """
    Hybrid Input-Output reduction cycle
    """
    def __init__(self, sock, *args, **kwargs):
        super(HIO, self).__init__(*args, **kwargs)
        self.sock = sock

    def __pow__(self, n):
        """

        :param n: a strictly positive integer
        :return: a new HIO operator with the number of cycles multiplied by n
        """
        assert isinstance(n, int) or isinstance(n, np.integer)
        return HIO(self.sock, beta=self.beta, positivity=self.positivity, calc_llk=self.calc_llk, nb_cycle=self.nb_cycle * n,
                   show_cdi=self.show_cdi, fig_num=self.fig_num, zero_mask=self.zero_mask)

    def op(self, cdi: CDI):
        t0 = timeit.default_timer()
        ic_dt = 0
        for ic in range(self.nb_cycle):
            calc_llk = False
            if self.calc_llk:
                if cdi.cycle % self.calc_llk == 0:
                    calc_llk = True
            cdi = HIOProj(beta=self.beta, positivity=self.positivity) * FourierApplyAmplitude(
                calc_llk=calc_llk, zero_mask=self.zero_mask) * CopyToPrevious() * cdi

            if calc_llk:
                # Average time/cycle over the last N cycles
                dt = (timeit.default_timer() - t0) / (ic - ic_dt + 1)
                ic_dt = ic + 1
                t0 = timeit.default_timer()
                llk = cdi.get_llk()
                cdi.update_history(mode='llk', dt=dt, algorithm='HIO', verbose=True)
            else:
                cdi.history.insert(cdi.cycle, algorithm='HIO')
            if self.show_cdi:
                if cdi.cycle % self.show_cdi == 0:
                    cdi = ShowCDI(self.sock) * cdi
            cdi.cycle += 1
        return cdi

class RAAR(_RAAR):
    """
    RAAR cycle
    """
    def __init__(self, sock, *args, **kwargs):
        super(RAAR, self).__init__(*args, **kwargs)
        self.sock = sock
    
    def __pow__(self, n):
        """

        :param n: a strictly positive integer
        :return: a new RAAR operator with the number of cycles multiplied by n
        """
        assert isinstance(n, int) or isinstance(n, np.integer)
        return RAAR(self.sock, beta=self.beta, positivity=self.positivity, calc_llk=self.calc_llk, nb_cycle=self.nb_cycle * n,
                    show_cdi=self.show_cdi, fig_num=self.fig_num, zero_mask=self.zero_mask)

    def op(self, cdi: CDI):
        t0 = timeit.default_timer()
        ic_dt = 0
        for ic in range(self.nb_cycle):
            calc_llk = False
            if self.calc_llk:
                if cdi.cycle % self.calc_llk == 0:
                    calc_llk = True

            cdi = RAARProj(self.beta, positivity=self.positivity) * FourierApplyAmplitude(
                calc_llk=calc_llk, zero_mask=self.zero_mask) * CopyToPrevious() * cdi

            if calc_llk:
                # Average time/cycle over the last N cycles
                dt = (timeit.default_timer() - t0) / (ic - ic_dt + 1)
                ic_dt = ic + 1
                t0 = timeit.default_timer()
                llk = cdi.get_llk()
                cdi.update_history(mode='llk', dt=dt, algorithm='RAAR', verbose=True)
            else:
                cdi.history.insert(cdi.cycle, algorithm='RAAR')
            if self.show_cdi:
                if cdi.cycle % self.show_cdi == 0:
                    cdi = ShowCDI(self.sock) * cdi
            cdi.cycle += 1
        return cdi


class GPS(_GPS):
    """
    GPS cycle, according to Pham et al [2019]
    """
    def __init__(self, sock, *args, **kwargs):
        super(GPS, self).__init__(*args, **kwargs)
        self.sock = sock

    def __pow__(self, n):
        """

        :param n: a strictly positive integer
        :return: a new GPS operator with the number of cycles multiplied by n
        """
        assert isinstance(n, int) or isinstance(n, np.integer)
        return GPS(self.sock, inertia=self.inertia, t=self.t, s=self.s, sigma_f=self.sigma_f, sigma_o=self.sigma_o,
                   positivity=self.positivity, calc_llk=self.calc_llk, nb_cycle=self.nb_cycle * n,
                   show_cdi=self.show_cdi, fig_num=self.fig_num, zero_mask=self.zero_mask)

    def op(self, cdi: CDI):
        t0 = timeit.default_timer()
        ic_dt = 0

        s = 1.0 / np.sqrt(cdi._cu_iobs.size)  # FFT scaling np.float32(1)

        epsilon = np.float32(self.inertia / (self.inertia + self.t))

        ny, nx = np.int32(cdi._obj.shape[-2]), np.int32(cdi._obj.shape[-1])
        if cdi._obj.ndim == 3:
            nz = np.int32(cdi._obj.shape[0])
        else:
            nz = np.int32(1)

        # Make sure we have tmp copy arrays available
        if has_attr_not_none(cdi, '_cu_z') is False:
            cdi._cu_z = cua.empty_like(cdi._cu_obj)
        elif cdi._cu_z.shape != cdi._cu_obj.shape:
            cdi._cu_z = cua.empty_like(cdi._cu_obj)

        if has_attr_not_none(cdi, '_cu_y') is False:
            cdi._cu_y = cua.empty_like(cdi._cu_obj)
        elif cdi._cu_y.shape != cdi._cu_obj.shape:
            cdi._cu_y = cua.empty_like(cdi._cu_obj)

        # We start in Fourier space (obj = z_0)
        cdi = FT(scale=True) * cdi

        # z_0 = FT(obj)
        cu_drv.memcpy_dtod(dest=cdi._cu_z.gpudata, src=cdi._cu_obj.gpudata, size=cdi._cu_obj.nbytes)

        # Start with obj = y_0 = 0
        cdi._cu_obj.fill(np.complex64(0))

        for ic in range(self.nb_cycle):
            calc_llk = False
            if self.calc_llk:
                if cdi.cycle % self.calc_llk == 0:
                    calc_llk = True

            # keep y copy
            cu_drv.memcpy_dtod(dest=cdi._cu_y.gpudata, src=cdi._cu_obj.gpudata, size=cdi._cu_obj.nbytes)

            cdi = FT(scale=False) * cdi

            # ^z = z_k - t F(y_k)
            self.processing_unit.cu_gps1(cdi._cu_obj, cdi._cu_z, self.t * s, self.sigma_o, nx, ny, nz)

            cdi = ApplyAmplitude(calc_llk=calc_llk, zero_mask=self.zero_mask) * cdi

            # obj = z_k+1 = (1 - epsilon) * sqrt(iobs) * exp(i * arg(^z)) + epsilon * z_k
            self.processing_unit.cu_gps2(cdi._cu_obj, cdi._cu_z, epsilon)

            if calc_llk:
                # Average time/cycle over the last N cycles
                dt = (timeit.default_timer() - t0) / (ic - ic_dt + 1)
                ic_dt = ic + 1
                t0 = timeit.default_timer()
                llk = cdi.get_llk()
                cdi.update_history(mode='llk', dt=dt, algorithm='GPS', verbose=True)
            else:
                cdi.history.insert(cdi.cycle, algorithm='GPS')
            if self.show_cdi:
                if cdi.cycle % self.show_cdi == 0:
                    cdi = IFT(scale=True) * cdi
                    cdi = ShowCDI(self.sock) * cdi
                    cdi = FT(scale=True) * cdi
            cdi.cycle += 1

            if ic < self.nb_cycle - 1:
                # obj = 2 * z_k+1 - z_k  & store z_k+1 in z
                self.processing_unit.cu_gps3(cdi._cu_obj, cdi._cu_z)

                cdi = IFT(scale=False) * cdi

                # obj = ^y = proj_support[y_k + s * obj] * G_sigma_f
                self.processing_unit.cu_gps4(cdi._cu_obj, cdi._cu_y, cdi._cu_support, self.s * s, self.sigma_f,
                                             self.positivity, nx, ny, nz)
            else:
                self.processing_unit.cu_scale(cdi._cu_obj, s)

        # Free memory
        cdi._cu_y.gpudata.free()
        cdi._cu_z.gpudata.free()
        del cdi._cu_y, cdi._cu_z

        # Back to object space
        cdi = IFT(scale=False) * cdi

        return cdi


class ML(_ML):
    """
    Maximum likelihood conjugate gradient minimization
    """

    def __init__(self, sock, *args, **kwargs):
        super(ML, self).__init__(*args, **kwargs)
        self.sock = sock

    def __pow__(self, n):
        """

        :param n: a strictly positive integer
        :return: a new ML operator with the number of cycles multiplied by n
        """
        assert isinstance(n, int) or isinstance(n, np.integer)
        return ML(self.sock, reg_fac=self.reg_fac, nb_cycle=self.nb_cycle * n, calc_llk=self.calc_llk, show_cdi=self.show_cdi,
                  fig_num=self.fig_num)

    def op(self, cdi: CDI):
        if self.need_init is False:
            if (has_attr_not_none(cdi, '_cu_obj_dir') is False) \
                    or (has_attr_not_none(cdi, '_cu_dpsi') is False) \
                    or (has_attr_not_none(cdi, '_cu_obj_grad') is False) \
                    or (has_attr_not_none(cdi, '_cu_obj_grad_last') is False):
                self.need_init = True

        if self.need_init:
            # Take into account support in regularization
            N = cdi._obj.size
            # Total number of photons
            Nph = cdi.iobs_sum
            cdi.llk_support_reg_fac = np.float32(self.reg_fac / (8 * N / Nph))
            # if cdi.llk_support_reg_fac > 0:
            #    print("Regularization factor for support:", cdi.llk_support_reg_fac)

            cdi._cu_obj_dir = cua.empty(cdi._obj.shape, np.complex64)
            cdi._cu_psi = cua.empty(cdi._obj.shape, np.complex64)
            # cdi._cu_dpsi = cua.empty(cdi._obj.shape, np.complex64)
            cdi._cu_obj_grad = cua.empty(cdi._obj.shape, np.complex64)
            cdi._cu_obj_gradlast = cua.empty(cdi._obj.shape, np.complex64)
            self.need_init = False

        ny, nx = np.int32(cdi._obj.shape[-2]), np.int32(cdi._obj.shape[-1])
        if cdi._obj.ndim == 3:
            nz = np.int32(cdi._obj.shape[0])
        else:
            nz = np.int32(1)

        t0 = timeit.default_timer()
        ic_dt = 0
        for ic in range(self.nb_cycle):
            calc_llk = False
            if self.calc_llk:
                if cdi.cycle % self.calc_llk == 0:
                    calc_llk = True

            cu_drv.memcpy_dtod(dest=cdi._cu_psi.gpudata, src=cdi._cu_obj.gpudata, size=cdi._cu_psi.nbytes)

            self.processing_unit.cu_fft_set_plan(cdi._cu_psi, batch=False)
            cu_fft.fft(cdi._cu_psi, cdi._cu_psi, self.processing_unit.cufft_plan)
            # TODO: avoid this separate normalization step !
            cdi._cu_psi *= 1 / np.sqrt(cdi.iobs.size)

            if calc_llk:
                cdi._cu_psi, cdi._cu_obj = cdi._cu_obj, cdi._cu_psi
                cdi._is_in_object_space = False
                cdi = LLK() * cdi
                cdi._cu_psi, cdi._cu_obj = cdi._cu_obj, cdi._cu_psi
                cdi._is_in_object_space = True

            # This calculates the conjugate of [(1 - iobs/icalc) * psi]
            self.processing_unit.cu_ml_poisson_psi_gradient(cdi._cu_psi, cdi._cu_obj_grad, cdi._cu_iobs, nx, ny, nz)

            self.processing_unit.cu_fft_set_plan(cdi._cu_obj_grad, batch=False)
            cu_fft.fft(cdi._cu_obj_grad, cdi._cu_obj_grad, self.processing_unit.cufft_plan)
            # TODO: avoid this separate normalization step
            cdi._cu_obj_grad *= 1 / np.sqrt(cdi.iobs.size)

            if cdi.llk_support_reg_fac > 0:
                self.processing_unit.cu_ml_poisson_reg_support_gradient(cdi._cu_obj, cdi._cu_obj_grad, cdi._cu_support,
                                                                        cdi.llk_support_reg_fac)

            if ic == 0:
                beta = np.float32(0)
                cu_drv.memcpy_dtod(dest=cdi._cu_obj_dir.gpudata, src=cdi._cu_obj_grad.gpudata,
                                   size=cdi._cu_obj_grad.nbytes)
            else:
                # Polak-Ribière CG coefficient
                tmp = self.processing_unit.cu_cg_polak_ribiere_red(cdi._cu_obj_grad, cdi._cu_obj_gradlast).get()
                if False:
                    g1 = cdi._cu_obj_grad.get()
                    g0 = cdi._cu_obj_gradlast.get()
                    A, B = (g1.real * (g1.real - g0.real) + g1.imag * (g1.imag - g0.imag)).sum(), (
                            g0.real * g0.real + g0.imag * g0.imag).sum()
                    cpubeta = A / B
                    print("betaPR: (GPU)=%8.4e  , (CPU)=%8.4e [%8.4e/%8.4e], dot(g0.g1)=%8e [%8e]" %
                          (tmp.real / tmp.imag, cpubeta, A, B, (g0 * g1).sum().real, (abs(g0) ** 2).sum().real))
                # Reset direction if beta<0 => beta=0
                beta = np.float32(max(0, tmp.real / max(1e-20, tmp.imag)))

            self.processing_unit.cu_ml_poisson_cg_linear(beta, cdi._cu_obj_dir, np.float32(-1), cdi._cu_obj_grad)
            # For next cycle
            cdi._cu_obj_grad, cdi._cu_obj_gradlast = cdi._cu_obj_gradlast, cdi._cu_obj_grad

            # Avoid using two memory allocations for obj_grad and dpsi
            cdi._cu_dpsi = cdi._cu_obj_grad

            cu_drv.memcpy_dtod(dest=cdi._cu_dpsi.gpudata, src=cdi._cu_obj_dir.gpudata, size=cdi._cu_dpsi.nbytes)

            self.processing_unit.cu_fft_set_plan(cdi._cu_dpsi)
            cu_fft.fft(cdi._cu_dpsi, cdi._cu_dpsi, self.processing_unit.cufft_plan)
            # TODO: avoid this separate normalization step
            cdi._cu_dpsi *= 1 / np.sqrt(cdi.iobs.size)

            if cdi.llk_support_reg_fac > 0:
                tmp = self.processing_unit.cdi_ml_poisson_gamma_support_red(cdi._cu_iobs, cdi._cu_psi, cdi._cu_dpsi,
                                                                            cdi._cu_obj, cdi._cu_obj_dir,
                                                                            cdi._cu_support,
                                                                            cdi.llk_support_reg_fac).get()
                gamma_n, gamma_d = tmp.real, tmp.imag
                gamma = np.float32(gamma_n / gamma_d)
            else:
                tmp = self.processing_unit.cdi_ml_poisson_gamma_red(cdi._cu_iobs, cdi._cu_psi, cdi._cu_dpsi).get()
                gamma_n, gamma_d = tmp.real, tmp.imag
                gamma = np.float32(gamma_n / gamma_d)

            self.processing_unit.cu_ml_poisson_cg_linear(np.float32(1), cdi._cu_obj, gamma, cdi._cu_obj_dir)

            if calc_llk:
                # Average time/cycle over the last N cycles
                dt = (timeit.default_timer() - t0) / (ic - ic_dt + 1)
                ic_dt = ic + 1
                t0 = timeit.default_timer()
                llk = cdi.get_llk()
                cdi.update_history(mode='llk', dt=dt, algorithm='ML', verbose=True)
            else:
                cdi.history.insert(cdi.cycle, algorithm='ML')
            if self.show_cdi:
                if cdi.cycle % self.show_cdi == 0:
                    cdi = ShowCDI(self.sock) * cdi

            cdi.cycle += 1

        return cdi

