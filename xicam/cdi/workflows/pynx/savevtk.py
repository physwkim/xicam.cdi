import h5py
import numpy as np


filename = 'temp6-2020-03-03T16-01-30_LLKf000.1136_LLK000.1520_SupportThreshold0.25000.cxi'
h5file = h5py.File(filename, 'r')

data = h5file['/entry_1/data_1/data'][()]
shape = data.shape
print("shape = ", shape)

amp = np.zeros((shape[0], shape[-2] *shape[-1]))
phase = np.zeros((shape[0], shape[-2] *shape[-1]))

idx = 0
for img in data:
    amp[idx] = np.abs(img).flatten()
    phase[idx] = np.angle(img).flatten()
    idx += 1

with open('save.vtk', 'w') as savefile:
    savefile.write('# vtk DataFile Version 2.0\n')
    savefile.write('Commnet goes here\n')
    savefile.write('ASCII\n')
    savefile.write('DATASET STRUCTURED_POINTS\n')
    savefile.write('DIMENSIONS    {}   {}   {}\n\n'.format(shape[-1], shape[-2], shape[0]))

    savefile.write('ORIGIN    0.000   0.000   0.000\n')
    savefile.write('SPACING    1.397637e+01   1.397637e+01   1.397637e+01\n\n')

    savefile.write('POINT_DATA    {}\n'.format(shape[0] * shape[1] * shape[2]))
    savefile.write('SCALARS amp double\n')
    savefile.write('LOOKUP_TABLE default\n\n')

    np.savetxt(savefile, amp)

    savefile.write('FIELD FieldData 1\n')
    savefile.write('phases 1 {} double\n'.format(shape[0] * shape[1] * shape[2]))

    np.savetxt(savefile, phase)
