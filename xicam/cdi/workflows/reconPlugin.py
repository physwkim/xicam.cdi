import os
import sys
import subprocess

from xicam.plugins import ProcessingPlugin, Input, Output

from xicam.cdi.workflows.pynx.pynx_bl9ccdi import FILE as cxipty


class ReconPlugin(ProcessingPlugin):
    args = Input(description='Reconstruction Parameters', type=dict)
    zmqLogPort = Input(description='ZeromMQ Log Port', type=int)
    zmqImgPort = Input(description='ZeromMQ Img Port', type=int)
    process = None
    
    def evaluate(self):
        command = [sys.executable, cxipty]
        command.extend(self.args.value)
        command.extend(['zmq_log_port='+str(self.zmqLogPort.value)])
        command.extend(['zmq_img_port='+str(self.zmqImgPort.value)])

        with subprocess.Popen(command) as run_ptycho:
            self.process = run_ptycho
