from xicam.core.execution.workflow import Workflow
from xicam.cdi.workflows.reconPlugin import ReconPlugin

class ReconWorkflow(Workflow):
    def __init__(self):
        super(ReconWorkflow, self).__init__('Preprocess')

        self.reconPlugin = ReconPlugin()

        self.processes = [self.reconPlugin]
        self.autoConnectAll()
