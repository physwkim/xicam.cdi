import numpy as np
import threading

from silx.gui import qt
from silx.gui.utils.concurrent import submitToQtMainThread as _submit

from xicam.core.data import NonDBHeader
from xicam.gui.widgets.previewwidget import PreviewWidget as _PreviewWidget


class PreviewWidget(_PreviewWidget):
    def __init__(self):
        super(PreviewWidget, self).__init__()

    def preview_header(self, header: NonDBHeader):
        try:
            data = header.meta_array()[0]
            _submit(self.setImage, data)
        except IndexError:
            self.imageitem.clear()
            self.setText("UNKNOWN DATA FORMAT")

    def setImage(self, imgdata):
        self.imgdata = imgdata

        thread = UpdateThread(self)
        thread.start()

class UpdateThread(threading.Thread):
    """Thread updating the table thread"""

    def __init__(self, parent):
        super(UpdateThread, self).__init__()
        self.parent = parent

    def start(self):
        """Start the update thread"""
        super(UpdateThread, self).start()

    def run(self):
        """Method implementing thread loop that updates the table"""

        imagedata = self.parent.imgdata
        data = np.log(imagedata * (imagedata > 0) + (imagedata < 1))

        # Run update asynchronously in Qt's main thread
        _submit(self.parent.imageitem.clear)
        _submit(self.parent.textitem.hide)
        _submit(self.parent.imageitem.setImage, data)
        _submit(self.parent.imageitem.setTransform, qt.QTransform(1, 0, 0, -1, 0, self.parent.imgdata.shape[-2]))
        _submit(self.parent.view.autoRange)

    def stop(self):
        """Stop the update thread"""
        self.join(2)
