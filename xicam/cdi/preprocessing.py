import sys
import os
import json
from collections import OrderedDict

from silx.gui import qt

import pyqtgraph.parametertree.parameterTypes as pTypes

from pyqtgraph.parametertree import Parameter, \
                                    ParameterTree, \
                                    ParameterItem

from xicam.cdi.pathParameter import FilePathParameter, \
                                             DirPathParameter, \
                                             LabelParameter, \
                                             SimpleParameter

defaultsFile = os.path.join(os.path.dirname(__file__), 'workflows','defaults_9C.json')
DEFAULTS = json.loads(open(defaultsFile).read())

class PreprocessingPanel(ParameterTree):
    sigDoPreprocess = qt.pyqtSignal()
    InImageType = OrderedDict([('tiff', None),
                               ('xim', None)])

    def __init__(self, parent, *args, **kwargs):
        super(PreprocessingPanel, self).__init__(*args, **kwargs)
        self.parent = parent

        # Parameters associated with the experiment
        self.expParams = LabelParameter(name='Experiment Parameters',
                                        type='str',
                                        readonly=True,
                                        value='')

        # Parameters associated with the path
        self.pathParams = LabelParameter(name='Path Parameters',
                                         type='str',
                                         readonly=True,
                                         value='')

        # Incident beam energy in eV
        self.energy = SimpleParameter(name='Energy',
                                      type='float',
                                      value=8000,
                                      step=0.01,
                                      decimals=10,
                                      siPrefix=True,
                                      suffix='eV')

        # AreaDetector pixel size in micrometers
        self.pixelSize = SimpleParameter(name='Pixel Size',
                                         type='float',
                                         value=DEFAULTS['detector']['pixelSize'],
                                         decimals=10,
                                         siPrefix=True,
                                         suffix='m')

        # Center pixel number along height
        self.cch1 = SimpleParameter(name='Center Pixel (height)',
                                    type='float',
                                    value=64,
                                    siPrefix=True,
                                    decimals=10,
                                    suffix='')

        # Center pixel number along width
        self.cch2 = SimpleParameter(name='Center Pixel (width)',
                                    type='float',
                                    value=64,
                                    siPrefix=True,
                                    decimals=10,
                                    suffix='')

        # Sample to detector distance in meters
        self.detectorDistance = SimpleParameter(name='Detector Distance',
                                                type='float',
                                                value=DEFAULTS['detector']['distance'],
                                                siPrefix=True,
                                                decimals=10,
                                                suffix='m')

        self.saturationLevel = SimpleParameter(name='Saturation Level',
                                               type='int',
                                               siPrefix=False,
                                               value=DEFAULTS['detector']['saturationThreshold'])



        self.imageSourceType = pTypes.ListParameter(name='Image Source Type',
                                                    type='list',
                                                    values=['Single Tiff', 'Directory'],
                                                    value='Single Tiff')

        self.imageSourceType.sigValueChanged.connect(self.toggleImgSourceParams)

        # Scan image directory
        self.imagePathFile = FilePathParameter(name='Image File',
                                               type='str',
                                               value='',
                                               dir=True,
                                               fileType='Image (*.tif *.tiff)')

        # Scan image directory
        self.imagePathDir = DirPathParameter(name='Image Directory',
                                             type='str',
                                             value='',
                                             dir=True)
        self.imagePathDir.show(False)

        self.expParams.addChild(self.energy)
        self.expParams.addChild(self.pixelSize)
        self.expParams.addChild(self.cch1)
        self.expParams.addChild(self.cch2)
        self.expParams.addChild(self.detectorDistance)

        self.pathParams.addChild(self.imageSourceType)
        self.pathParams.addChild(self.imagePathDir)
        self.pathParams.addChild(self.imagePathFile)

        # Support Type
        self.mask = SimpleParameter(name='Mask',
                                    type='bool',
                                    value=False)
        # Noise level counts
        self.noiseLevel = SimpleParameter(name='Noise Level(set to zero)',
                                          type='int',
                                          value=DEFAULTS['detector']['noiseLevel'],
                                          siPrefix=True,
                                          suffix='')

        self.mask.addChild(self.noiseLevel)
        self.mask.addChild(self.saturationLevel)

        # QConversion
        self.qconvParams = LabelParameter(name='QConversion',
                                          type='str',
                                          readonly=True,
                                          value='')

        # delta angle
        self.delta = SimpleParameter(name='delta',
                                     type='float',
                                     value=18.807,
                                     step=0.001,
                                     decimals=10,
                                     siPrefix=True,
                                     suffix='deg.')

        # eta angle
        self.eta = SimpleParameter(name='eta',
                                   type='float',
                                   value=18.807/2.,
                                   step=0.001,
                                   decimals=10,
                                   siPrefix=True,
                                   suffix='deg.')

        # chi angle
        self.chi = SimpleParameter(name='chi',
                                   type='float',
                                   value=0,
                                   step=0.001,
                                   decimals=10,
                                   siPrefix=True,
                                   suffix='deg.')

        # phi angle
        self.phi = SimpleParameter(name='phi',
                                   type='float',
                                   value=0,
                                   step=0.001,
                                   decimals=10,
                                   siPrefix=True,
                                   suffix='deg.')

        # nu angle
        self.nu = SimpleParameter(name='nu',
                                  type='float',
                                  value=0,
                                  step=0.001,
                                  decimals=10,
                                  siPrefix=True,
                                  suffix='deg.')

        # mu angle
        self.mu = SimpleParameter(name='mu',
                                  type='float',
                                  value=0,
                                  step=0.001,
                                  decimals=10,
                                  siPrefix=True,
                                  suffix='deg.')

        # rocking step angle (eta)
        self.eta_step = SimpleParameter(name='rocking angle(eta)',
                                        type='float',
                                        value=0.005,
                                        step=0.0001,
                                        decimals=10,
                                        siPrefix=True,
                                        suffix='deg.')

        # rocking step angle (phi)
        self.phi_step = SimpleParameter(name='rocking angle(phi)',
                                        type='float',
                                        value=0,
                                        step=0.0001,
                                        decimals=10,
                                        siPrefix=True,
                                        suffix='deg.')
        
        self.qconvParams.addChild(self.delta)
        self.qconvParams.addChild(self.eta)
        self.qconvParams.addChild(self.chi)
        self.qconvParams.addChild(self.phi)
        self.qconvParams.addChild(self.nu)
        self.qconvParams.addChild(self.mu)
        self.qconvParams.addChild(self.eta_step)
        self.qconvParams.addChild(self.phi_step)

        self.comment = pTypes.TextParameter(name='Comments',
                                         readonly=False,
                                         value="")

        self.preprocessAction = pTypes.ActionParameter(name='Preprocess')
        self.preprocessAction.sigActivated.connect(self.sigDoPreprocess)

        self.qconvParams.addChild(self.preprocessAction)

        self.parameter = pTypes.GroupParameter(name='Preprocessing',
                                               children=[self.expParams,
                                                         self.pathParams, 
                                                         self.mask,
                                                         self.qconvParams,
                                                         self.comment])

        self.setParameters(self.parameter, showTop=False)


    def toggleImgSourceParams(self, param, value):
        if value == 'Single Tiff':
            self.imagePathDir.show(False)
            self.imagePathFile.show(True)
        else:
            self.imagePathDir.show(True)
            self.imagePathFile.show(False)

